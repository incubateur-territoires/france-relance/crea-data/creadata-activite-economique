![](ressources/logo_geo16crea.png)

# Recensement de l'Activité Économique

Ce dépot recense l'ensemble des fichiers permettant la mise en place d'une base de données géographique permettant la gestion de l'activité économique d'un territoire.

L’article 220 de la loi « climat et résilience » du 22 août 2021 rend obligatoire la tenue d’un inventaire des zones d’activités économiques nécessitant la refonte du modèle pour coller au plus près des obligations de la loi.

Ce modèle est mis en oeuvre par l'ATD16 dans le cadre d'une utilisation sur le logiciel X'MAP développé par la société SIRAP. Il s'implémente dans un serveur de base de base de données PostgreSQL/PostGIS. 
Néanmoins il peut être adapté pour un usage sur un autre outil.

Le modèle est susceptible d'évoluer en fonction des besoins des collectivités.

# Principe de fonctionnement

Des tables géographiques permettent d'**inventorier** :
- commerces : élément ponctuel permettant de localiser un batiment à vocation commerciale (type d'activité, statut d'occupation, ...)
- site de la zone d'activités économique (ZAE) : élément surfacique représentant l'emprise géographique de la ZAE, comprenant un ou plusieurs lots (ou unités foncières)
- unité foncière ou lot : élément surfacique représentant un "îlot" d'un seul tenant composé d'une ou plusieurs parcelles appartenant à un même propriétaire ou à la même indivision
- établissement : élément ponctuel localisant un établissement économique
- destination économique : élément surfacique représentant une zone homogène ayant un usage unique et défini (entreprise, équipement, équipement public, réserve foncière, friche, etc.)

**Calcul du taux de vacance** :  

La loi indique au moins une façon de calculer le taux de vacance (3° de l'art. L.318-8-2 du CU) :  
>*"Le taux de vacance de la zone d'activité économique, calculé en rapportant le nombre total d'unités foncières de la zone d'activité au nombre d'unités foncières qui ne sont plus affectées à une activité assujettie à la cotisation foncière des entreprises prévue à l'article 1447 du code général des impôts depuis au moins deux ans au 1er janvier de l'année d'imposition et qui sont restées inoccupées au cours de la même période."*  

L'échelle est l'unité foncière. La base de données LOCOMVAC, gérée par la DGFIP, qui s'appuie sur le non-paiement de la CFE, semble être à privilégier.  
Pour le moment, sans accès à cette base LOCOMVAC, le calcul du taux de vacance est simplifié dans ce modèle. Il s'effectue via une vue de la couche des sites ZAE comme suit :  

![](ressources/calcul_taux_vacance.png)

# Génèse : quelques dates

* 2018 : mise en production d'une pseudo gestion localisation des commerces pour les Managers de Centre-Bourg (test CC4B).
* mai 2021-octobre 2021 : développement d'un nouveau modèle de données pour répondre aux besoins des collectivités.
* septembre 2022-décembre 2022 : refonte du nouveau modèle de données pour le recensement des zones d'activités économiques afin de répondre à l'article 220 de la loi « climat et résilience » du 22 août 2021.
* janvier 2023 : mise en production du modèle dans X'MAP.

# Ressources du projet

* MCD [[pdf](ressources/mcd_economie.drawio.pdf)]  
* Schéma applicatif [[pdf](ressources/schema_applicatif_economie.drawio.pdf)]  

/!\ le schéma de bdd "atd16_economie" est utilisé par l'ATD16 pour répondre aux exigences de son schéma applicatif. La modification de cette valeur nécessitera d'intervenir sur l'ensemble des fichiers de code.

- Script agrégé de l'ensemble de la structure [[sql](donn%C3%A9es/bdd/aggregat_atd16_economie.sql)]
- Schéma **atd16_economie** [[sql](donn%C3%A9es/bdd/000_create_schema_atd16_economie.sql)]
   * Fonctions-triggers génériques [[sql](donn%C3%A9es/bdd/001_create_function_trigger_generique.sql)]
    

## Commerces

   * Tables de listes déroulantes :
      * Table **lst_statut_local_com** [[sql](donn%C3%A9es/bdd/020_lst_statut_local_com.sql)]  
   * Table géographique :
      * Table **geo_commerce** [[sql](donn%C3%A9es/bdd/021_geo_commerce.sql)]  
         * Représentation graphique [[json](donn%C3%A9es/representation_graphique/json/atd16_economie_geo_commerce.json)]

## Zone d'activités économiques

   * Tables de listes déroulantes :
      * Table **lst_vocation_site** [[sql](donn%C3%A9es/bdd/051_lst_vocation_site.sql)]  
      * Table **lst_type_site** [[sql](donn%C3%A9es/bdd/052_lst_type_site.sql)]  
      * Table **lst_etat_site** [[sql](donn%C3%A9es/bdd/053_lst_etat_site.sql)]  
      * Table **lst_disponibilite_lot** [[sql](donn%C3%A9es/bdd/054_lst_disponibilite_lot.sql)]  
      * Table **lst_type_destination** [[sql](donn%C3%A9es/bdd/055_lst_type_destination.sql)]  
   * Tables géographiques :
      * Table **geo_site_eco** [[sql](donn%C3%A9es/bdd/101_geo_site_eco.sql)]  
         * Fonction-trigger dépendante [[sql](donn%C3%A9es/bdd/102_function_trigger_depend_geo_site_eco.sql)]
         * Fonctions-triggers associées [[sql](donn%C3%A9es/bdd/301_trigger_geo_site_eco.sql)]
         * Représentation graphique [[json](donn%C3%A9es/representation_graphique/json/atd16_economie_geo_site_eco.json)] - [[sld](donn%C3%A9es/representation_graphique/sld/v_geo_site_eco.sld)]
         * Représentation textuelle [[sld](donn%C3%A9es/representation_graphique/sld/geo_site_eco_tpn.sld)]
      * Table **geo_lot_eco** [[sql](donn%C3%A9es/bdd/121_geo_lot_eco.sql)]  
         * Fonction-trigger dépendante [[sql](donn%C3%A9es/bdd/122_function_trigger_depend_geo_lot_eco.sql)]
         * Fonctions-triggers associées [[sql](donn%C3%A9es/bdd/321_trigger_geo_lot_eco.sql)]
         * Représentation graphique [[json](donn%C3%A9es/representation_graphique/json/atd16_economie_geo_lot_eco.json)] - [[sld](donn%C3%A9es/representation_graphique/sld/geo_lot_eco.sld)]
      * Table **geo_destination_eco** [[sql](donn%C3%A9es/bdd/141_geo_destination_eco.sql)]  
         * Représentation graphique [[json](donn%C3%A9es/representation_graphique/json/atd16_economie_geo_destination_eco.json)] - [[sld](donn%C3%A9es/representation_graphique/sld/geo_destination_eco.sld)]
      * Table **geo_etablissement_eco** [[sql](donn%C3%A9es/bdd/161_geo_etablissement_eco.sql)]  
         * Fonctions-triggers associées [[sql](donn%C3%A9es/bdd/361_trigger_geo_etablissement_eco.sql)]
         * Représentation graphique [[json](donn%C3%A9es/representation_graphique/json/atd16_economie_geo_etablissement_eco.json)] - [[sld](donn%C3%A9es/representation_graphique/sld/geo_etablissement_eco.sld)]


# Ressources annexes

* Notice d'utilisation dans X'MAP [[pdf](ressources/Géo16Créa_-_Économie.pdf)]

# Ressources générales


