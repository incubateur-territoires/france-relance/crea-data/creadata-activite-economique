-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/09/28 : RL / Création du fichier sur Git
--                 . Ajout de la fonction-trigger update_created_column()
--                 . Ajout de la fonction-trigger update_modified_column()
--                 . Ajout de la fonction-trigger maj_refcad_commerce()
--                 . Ajout de la fonction-trigger setXY_coord()
--                 . Ajout de la fonction-trigger maj_id_bati_ign()
-- 2022/09/29 : SL / Ajout de la fonction-trigger f_maj_sup_m2()
-- 2022/10/20 : SL / Modification de la fonction-trigger update_created_column() en f_date_creation()
--                 . Modification de la fonction-trigger update_modified_column() en f_date_maj()
--                 . Modification de la fonction-trigger setXY_coord() en f_maj_xy_coord()
--                 . Ajout de la fonction-trigger f_recup_nom_1er_proprio()
-- 2022/11/07 : SL / Correction de la fonction-trigger f_maj_sup_m2()
-- 2022/11/30 : SL / Correction de la fonction-trigger f_maj_sup_m2()
-- 2022/12/07 : SL / Correction de la fonction-trigger maj_id_bati_ign()

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                     Fonction(s) trigger(s) générique(s)                                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_economie.f_date_creation();

CREATE FUNCTION atd16_economie.f_date_creation()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    NEW.date_creation = now(); -- À la création d'un objet, le champ date_creation prend la valeur de la date du jour
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_economie.f_date_creation() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_date_creation() IS 'Fonction trigger permettant l''initialisation du champ date_creation';


-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_economie.f_date_maj();

CREATE FUNCTION atd16_economie.f_date_maj()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    NEW.date_maj = now(); -- À la modification d'un objet, le champ date_maj prend la valeur de la date du jour
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_economie.f_date_maj() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_date_maj() IS 'Fonction trigger permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                 Récupération des reférences parcellaires                                                   ###
-- ##################################################################################################################################################

-- DROP FUNCTION IF EXISTS atd16_economie.maj_refcad_commerce();

CREATE OR REPLACE FUNCTION atd16_economie.maj_refcad_commerce()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
 

BEGIN 
    
    select parc.ident 
	from pci.v_geo_parcelle_encours as parc 
	where ST_Intersects(ST_SetSRID(NEW.the_geom,0), parc.the_geom) 
	-- Le ST_SetSRID à 0 est nécessaire dans Postgis 3. En effet les nouveaux objets arrivent en 4326.
	into NEW.ref_cada;
    return NEW;

END;
$BODY$;

ALTER FUNCTION atd16_economie.maj_refcad_commerce()
    OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.maj_refcad_commerce() IS 'Fonction trigger permettant la récupération de la référence cadastrale de la parcelle';    


-- ##################################################################################################################################################
-- ###                                                 Récupération des coordonnées géographiques                                                 ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_economie.f_maj_xy_coord();

CREATE OR REPLACE FUNCTION atd16_economie.f_maj_xy_coord()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    NEW.x_l93 := ST_X(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154)); -- On récupère le X, on le définit en 3857, puis on le met en 2154
    NEW.y_l93 := ST_Y(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154)); -- On récupère le Y, on le définit en 3857, puis on le met en 2154
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_economie.f_maj_xy_coord() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_xy_coord() IS 
    'Fonction trigger permettant la récupération des coordonnées géographiques';


-- ##################################################################################################################################################
-- ###                                                 Récupération de l'ID batiment de la BD Topo                                                 ###
-- ##################################################################################################################################################
	
-- DROP FUNCTION atd16_economie.maj_id_bati_ign();
	
CREATE OR REPLACE FUNCTION atd16_economie.maj_id_bati_ign()
  RETURNS trigger AS
$BODY$ 

BEGIN 
    
    select bati.cleabs 
	from ign_bdtopo.geo_batiment as bati 
	where ST_Intersects(ST_SetSRID(NEW.the_geom,3857), bati.the_geom) 
	-- Le ST_SetSRID à 0 est nécessaire dans Postgis 3. En effet les nouveaux objets arrivent en 4326. (obsolète raison inconnu, cela marche en 3857)
	into NEW.id_bati_ign;
    return NEW;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION atd16_economie.maj_id_bati_ign()
  OWNER TO sditecgrp;


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ sup_m2                                                         ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_economie.f_maj_sup_m2();

CREATE FUNCTION atd16_economie.f_maj_sup_m2()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    IF NEW.sup_m2 IS NULL -- Si sup_m2 est null
    THEN --Alors
        NEW.sup_m2 = ST_Area(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154)); -- Calcul de la surface à partir de la géométrie 
        -- reprojection car en 3857 la surface est à peu près deux fois plus grande
    END IF; 
	-- Il n'y a pas de modification lorsqu'une valeur (autre que NULL) est ajouté dans le champ sup_m2
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_sup_m2() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_sup_m2() IS 'Fonction trigger permettant la mise à jour du champ sup_m2';


-- ##################################################################################################################################################
-- ###                                                   Récupération du premier propriétaire                                                     ###
-- ##################################################################################################################################################

-- DROP FUNCTION IF EXISTS atd16_economie.f_recup_nom_1er_proprio();

CREATE OR REPLACE FUNCTION atd16_economie.f_recup_nom_1er_proprio()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
 
BEGIN 
    
    SELECT parc.nom 
	FROM pci.v_geo_parcelle_encours as parc 
	WHERE ST_Intersects(ST_SetSRID(NEW.the_geom,0), parc.the_geom) 
	-- Le ST_SetSRID à 0 est nécessaire dans Postgis 3. En effet les nouveaux objets arrivent en 4326.
	INTO NEW.proprietaire;
    RETURN NEW;

END;
$BODY$;

ALTER FUNCTION atd16_economie.f_recup_nom_1er_proprio() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_recup_nom_1er_proprio() IS 
    'Fonction trigger permettant la récupération du premier propriétaire de la parcelle cadastrale';    


