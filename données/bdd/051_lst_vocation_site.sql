-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/09/29 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                    Table non géographique : Domaine de valeur de la vocation du site                                       ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_economie.lst_vocation_site

CREATE TABLE atd16_economie.lst_vocation_site 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code du domaine de valeur
    libelle varchar(254), --[ATD16] Libellé du domaine de valeur
    CONSTRAINT pk_lst_vocation_site PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_economie.lst_vocation_site OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_economie.lst_vocation_site TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_economie.lst_vocation_site IS '[ATD16] Domaine de valeur de la vocation du site';

COMMENT ON COLUMN atd16_economie.lst_vocation_site.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_economie.lst_vocation_site.code IS '[ATD16] Code du domaine de valeur';
COMMENT ON COLUMN atd16_economie.lst_vocation_site.libelle IS '[ATD16] Libellé du domaine de valeur';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_economie.lst_vocation_site 
    (code, libelle)
VALUES
    ('00', 'Non renseigné'),
    ('01', 'ZI - zone industrielle'),
    ('02', 'ZA - zone artisanale'),
    ('03', 'ZC - zone commerciale'),
    ('04', 'ZM - zone mixte');

