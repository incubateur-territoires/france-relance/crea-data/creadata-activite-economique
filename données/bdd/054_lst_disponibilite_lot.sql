-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/09/29 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                  Table non géographique : Domaine de valeur de la disponibilité du lot                                     ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_economie.lst_disponibilite_lot

CREATE TABLE atd16_economie.lst_disponibilite_lot 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code du domaine de valeur
    libelle varchar(254), --[ATD16] Libellé du domaine de valeur
    CONSTRAINT pk_lst_disponibilite_lot PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_economie.lst_disponibilite_lot OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_economie.lst_disponibilite_lot TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_economie.lst_disponibilite_lot IS '[ATD16] Domaine de valeur de la disponibilité du lot';

COMMENT ON COLUMN atd16_economie.lst_disponibilite_lot.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_economie.lst_disponibilite_lot.code IS '[ATD16] Code du domaine de valeur';
COMMENT ON COLUMN atd16_economie.lst_disponibilite_lot.libelle IS '[ATD16] Libellé du domaine de valeur';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_economie.lst_disponibilite_lot 
    (code, libelle)
VALUES
    ('00', 'Non renseigné'),
    ('01', 'Non bâti'),
    ('02', 'Bâti vacant'),
    ('03', 'Occupé, sous optimisé ou potentiellement divisable'),
    ('04', 'Occupé et optimisé');

