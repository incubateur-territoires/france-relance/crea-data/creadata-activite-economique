-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/09/28 : RL / Création du fichier du schéma Economie


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                    Création du schéma : Economie                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- SCHEMA: atd16_economie

-- DROP SCHEMA atd16_economie ;

CREATE SCHEMA atd16_economie
  AUTHORIZATION sditecgrp;

COMMENT ON SCHEMA atd16_economie
    IS 'Gestion de l''activité économique';

GRANT ALL ON SCHEMA atd16_economie TO sditecgrp;


ALTER DEFAULT PRIVILEGES IN SCHEMA atd16_economie
    GRANT INSERT, SELECT, UPDATE, DELETE, TRUNCATE, REFERENCES, TRIGGER ON TABLES
    TO sditecgrp;

ALTER DEFAULT PRIVILEGES IN SCHEMA atd16_economie
    GRANT SELECT, UPDATE, USAGE ON SEQUENCES
    TO sditecgrp;

ALTER DEFAULT PRIVILEGES IN SCHEMA atd16_economie
    GRANT EXECUTE ON FUNCTIONS
    TO sditecgrp;

