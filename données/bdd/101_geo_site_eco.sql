-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/09/29 : SL / Création du fichier sur Git
--                 . Ajout du trigger 00_t_before_i_init_date_creation
--                 . Ajout du trigger 02_t_before_u_date_maj
--                 . Ajout du trigger 10_t_before_iu_maj_sup_m2
-- 2022/10/20 : SL / Correction du trigger 02_t_before_u_date_maj
--                 . Ajout du champ nb_unite_fonciere
--                 . Ajout du champ taux_vacance
-- 2022/11/03 : SL / Correction du champ taux_vacance en numeric(5,2)
-- 2022/11/09 : SL / Suppression du champ taux_vacance
--                 . Ajout de la vue v_geo_site_eco avec calcul du taux_vacance
-- 2022/12/05 : SL / Ajout du champ observation

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                  Table géographique : Sites économiques / Zones d'Activité Économiques                                     ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_economie.geo_site_eco;

CREATE TABLE atd16_economie.geo_site_eco
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    ident varchar(254), --[SIRAP] Libellé du site - Identifiant SIRAP
    vocation_site varchar(2), --[FK][ATD16] Identifiant de la vocation du site
    type_site varchar(2), --[FK][ATD16] Identifiant du type du site
    etat_site varchar(2), --[FK][ATD16] Identifiant de l'état du site
    sup_m2 integer, --[ATD16] Superficie du site en m² (semi-automatique)
    sup_foncier_dispo integer, --[ATD16] Surface foncière disponible sur le site en m² (automatique)
    nb_unite_fonciere integer, --[ATD16] Nombre d'unités foncières (lots) sur le site (automatique)
    nb_etablissement integer, --[ATD16] Nombre d'établissements (occupants) sur le site (automatique)
    annee_creation integer, --[ATD16] Année de création du site
    observation varchar(254), --[ATD16] Observations sur la zone
    date_creation timestamp, --[ATD16] Date de création de l'objet (automatique)
    date_maj timestamp, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_site_eco PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_economie.geo_site_eco OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_economie.geo_site_eco TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_economie.geo_site_eco IS '[ATD16] Table contenant la géographie des sites économiques (ZAE)';

COMMENT ON COLUMN atd16_economie.geo_site_eco.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_economie.geo_site_eco.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_economie.geo_site_eco.ident IS '[SIRAP] Libellé du site - Identifiant SIRAP';
COMMENT ON COLUMN atd16_economie.geo_site_eco.vocation_site IS '[FK][ATD16] Identifiant de la vocation du site';
COMMENT ON COLUMN atd16_economie.geo_site_eco.type_site IS '[FK][ATD16] Identifiant du type du site';
COMMENT ON COLUMN atd16_economie.geo_site_eco.etat_site IS '[FK][ATD16] Identifiant de l''état du site';
COMMENT ON COLUMN atd16_economie.geo_site_eco.sup_m2 IS '[ATD16] Superficie du site en m² (semi-automatique)';
COMMENT ON COLUMN atd16_economie.geo_site_eco.sup_foncier_dispo IS '[ATD16] Surface foncière disponible sur le site en m² (automatique)';
COMMENT ON COLUMN atd16_economie.geo_site_eco.nb_unite_fonciere IS '[ATD16] Nombre d''unités foncières (lots) sur le site (automatique)';
COMMENT ON COLUMN atd16_economie.geo_site_eco.nb_etablissement IS '[ATD16] Nombre d''établissements (occupants) sur le site (automatique)';
COMMENT ON COLUMN atd16_economie.geo_site_eco.annee_creation IS '[ATD16] Année de création du site';
COMMENT ON COLUMN atd16_economie.geo_site_eco.observation IS '[ATD16] Observations sur la zone';
COMMENT ON COLUMN atd16_economie.geo_site_eco.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_economie.geo_site_eco.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_economie.geo_site_eco.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                             v_geo_site_eco : vue des sites économiques (ZAE) avec le calcul du taux de vacance                             ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_economie.v_geo_site_eco;

CREATE OR REPLACE VIEW atd16_economie.v_geo_site_eco 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.ident,
        a.vocation_site,
        a.type_site,
        a.etat_site,
        a.sup_m2,
        a.sup_foncier_dispo,
        a.nb_unite_fonciere,
        a.nb_etablissement,
        CASE 
            WHEN a.nb_unite_fonciere::numeric = 0 THEN NULL
            ELSE (((a.nb_unite_fonciere::numeric - a.nb_etablissement::numeric) / a.nb_unite_fonciere::numeric) * 100)::numeric(5,2)
        END taux_vacance,
        a.annee_creation,
        a.observation,
        a.date_creation,
        a.date_maj,
        a.the_geom
    FROM atd16_economie.geo_site_eco a;

ALTER TABLE atd16_economie.v_geo_site_eco OWNER TO sditecgrp;

COMMENT ON VIEW atd16_economie.v_geo_site_eco IS 
    '[ATD16] Vue listant les sites économiques (ZAE) avec le calcul du taux de vacance de la ZAE telle que défini dans l''article L318-8-2 du Code de l''urbanisme';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                          Trigger(s) générique(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER "00_t_before_i_init_date_creation" ON atd16_economie.geo_site_eco;

CREATE TRIGGER "00_t_before_i_init_date_creation"
    BEFORE INSERT
    ON atd16_economie.geo_site_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_date_creation();
    
COMMENT ON TRIGGER "00_t_before_i_init_date_creation" ON atd16_economie.geo_site_eco IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER "02_t_before_u_date_maj" ON atd16_economie.geo_site_eco;

CREATE TRIGGER "02_t_before_u_date_maj"
    BEFORE UPDATE
    ON atd16_economie.geo_site_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_date_maj();
    
COMMENT ON TRIGGER "02_t_before_u_date_maj" ON atd16_economie.geo_site_eco IS
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ sup_m2                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER "10_t_before_iu_maj_sup_m2" ON atd16_economie.geo_site_eco;

CREATE TRIGGER "10_t_before_iu_maj_sup_m2"
    BEFORE INSERT OR UPDATE
    ON atd16_economie.geo_site_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_sup_m2();
    
COMMENT ON TRIGGER "10_t_before_iu_maj_sup_m2" ON atd16_economie.geo_site_eco IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ sup_m2';

