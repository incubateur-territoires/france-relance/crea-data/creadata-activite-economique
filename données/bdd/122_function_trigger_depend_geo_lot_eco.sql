-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/10/05 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_maj_idlot()

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                     Fonction(s) trigger(s) dépendante(s)                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idlot                                                         ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_economie.f_maj_idlot();

CREATE FUNCTION atd16_economie.f_maj_idlot()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    SELECT gid -- on sélectionne le champ gid
    FROM atd16_economie.geo_lot_eco AS lot_eco -- de la table geo_lot_eco
    WHERE ST_Intersects(NEW.the_geom, lot_eco.the_geom) -- qui intersecte le nouvel objet créé
    INTO NEW.idlot; -- et on insère la valeur de gid dans le nouveau champ idlot
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_economie.f_maj_idlot() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_idlot() IS 'Fonction trigger permettant la mise à jour du champ idlot';

