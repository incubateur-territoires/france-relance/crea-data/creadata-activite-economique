-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/10/05 : SL / Création du fichier sur Git
--                 . Ajout du trigger 00_t_before_i_init_date_creation
--                 . Ajout du trigger 02_t_before_u_date_maj
--                 . Ajout du trigger 10_t_before_iu_maj_sup_m2
--                 . Ajout du trigger 20_t_before_iu_maj_idsite
-- 2022/10/20 : SL / Correction du trigger 02_t_before_u_date_maj

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                               Table géographique : Destination économique                                                  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_economie.geo_destination_eco;

CREATE TABLE atd16_economie.geo_destination_eco
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idsite integer, --[FK][ATD16] Identifiant du site économique
    insee varchar(6), --[ATD16] Code INSEE de la commune
    ident varchar(254), --[SIRAP] Nom de la destination - Identifiant SIRAP
    type_destination varchar(2), --[FK][ATD16] Identifiant du type de destination
    sup_m2 integer, --[ATD16] Superficie de la destination en m² (semi-automatique)
    date_creation timestamp, --[ATD16] Date de création de l'objet (automatique)
    date_maj timestamp, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_destination_eco PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_economie.geo_destination_eco OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_economie.geo_destination_eco TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_economie.geo_destination_eco IS '[ATD16] Table contenant la géographie des destinations économiques ';

COMMENT ON COLUMN atd16_economie.geo_destination_eco.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_economie.geo_destination_eco.idsite IS '[FK][ATD16] Identifiant du site économique';
COMMENT ON COLUMN atd16_economie.geo_destination_eco.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_economie.geo_destination_eco.ident IS '[SIRAP] Nom de la destination - Identifiant SIRAP';
COMMENT ON COLUMN atd16_economie.geo_destination_eco.type_destination IS '[FK][ATD16] Identifiant du type de destination';
COMMENT ON COLUMN atd16_economie.geo_destination_eco.sup_m2 IS '[ATD16] Superficie de la destination en m² (semi-automatique)';
COMMENT ON COLUMN atd16_economie.geo_destination_eco.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_economie.geo_destination_eco.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_economie.geo_destination_eco.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################



-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                          Trigger(s) générique(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER "00_t_before_i_init_date_creation" ON atd16_economie.geo_destination_eco;

CREATE TRIGGER "00_t_before_i_init_date_creation"
    BEFORE INSERT
    ON atd16_economie.geo_destination_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_date_creation();
    
COMMENT ON TRIGGER "00_t_before_i_init_date_creation" ON atd16_economie.geo_destination_eco IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER "02_t_before_u_date_maj" ON atd16_economie.geo_destination_eco;

CREATE TRIGGER "02_t_before_u_date_maj"
    BEFORE UPDATE
    ON atd16_economie.geo_destination_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_date_maj();
    
COMMENT ON TRIGGER "02_t_before_u_date_maj" ON atd16_economie.geo_destination_eco IS
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ sup_m2                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER "10_t_before_iu_maj_sup_m2" ON atd16_economie.geo_destination_eco;

CREATE TRIGGER "10_t_before_iu_maj_sup_m2"
    BEFORE INSERT OR UPDATE
    ON atd16_economie.geo_destination_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_sup_m2();
    
COMMENT ON TRIGGER "10_t_before_iu_maj_sup_m2" ON atd16_economie.geo_destination_eco IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ sup_m2';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idsite                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER "20_t_before_iu_maj_idsite" ON atd16_economie.geo_destination_eco;

CREATE TRIGGER "20_t_before_iu_maj_idsite"
    BEFORE INSERT OR UPDATE
    ON atd16_economie.geo_destination_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_idsite();
    
COMMENT ON TRIGGER "20_t_before_iu_maj_idsite" ON atd16_economie.geo_destination_eco IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idsite';

