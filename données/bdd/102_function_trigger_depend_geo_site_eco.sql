-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/10/05 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_maj_idsite()
--                 . Ajout de la fonction f_maj_taux_vacance()
-- 2022/11/03 : SL / Correction de la fonction f_maj_taux_vacance()
-- 2022/11/09 : SL / Suppression de la fonction f_maj_taux_vacance()

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                     Fonction(s) trigger(s) dépendante(s)                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###              Mise à jour du champ idsite à la création ou la modification d'un lot, d'une destination ou d'un établissement                ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_economie.f_maj_idsite();

CREATE FUNCTION atd16_economie.f_maj_idsite()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    SELECT gid -- on sélectionne le champ gid
    FROM atd16_economie.geo_site_eco AS site_eco -- de la table geo_site_eco
    WHERE ST_Intersects(NEW.the_geom, site_eco.the_geom) -- qui intersecte le nouvel objet créé
    INTO NEW.idsite; -- et on insère la valeur de gid dans le nouveau champ idsite
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_economie.f_maj_idsite() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_idsite() IS
    'Fonction trigger permettant la mise à jour du champ idsite lors de la création ou la modification d''un lot, d''une destination ou d''un établissement';


