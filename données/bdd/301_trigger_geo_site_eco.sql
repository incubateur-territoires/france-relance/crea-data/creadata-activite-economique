-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/10/05 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_maj_idsite_2() et du trigger associé 40_t_after_iud_maj_idsite_2
-- 2022/10/20 : SL / Ajout de la fonction f_supp_valeur_idsite() et du trigger associé t_after_d_site_supp_idsite
-- 2022/11/04 : SL / Correction de la fonction f_maj_idsite_2() : maj_idsite si UPDATE géographique
--                 . Ajout de la fonction f_maj_nb_unite_fonciere_2() et du trigger associé 30_t_after_iu_maj_nb_unite_fonciere_2
--                 . Ajout de la fonction f_maj_nb_etablissement_2() et du trigger associé 20_t_after_iu_maj_nb_etablissement_2
--                 . Ajout de la fonction f_maj_taux_vacance_2() et du trigger associé t_after_iu_maj_taux_vacance_2
-- 2022/11/07 : SL / Correction de la fonction f_maj_taux_vacance_2()
--                 . Correction de la fonction f_maj_nb_etablissement_2()
--                 . Correction de la fonction f_maj_nb_unite_fonciere_2()
-- 2022/11/09 : SL / Suppression de la fonction f_maj_taux_vacance_2()
--                 . Suppression de la fonction f_supp_valeur_idsite() et du trigger associé t_after_d_site_supp_idsite

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                      Fonctions triggers et triggers spécifiques à la table geo_site_eco                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                   Mise à jour du lien idsite lorsqu'un site est créé, modifié ou supprimé                                  ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_economie.f_maj_idsite_2();

CREATE OR REPLACE FUNCTION atd16_economie.f_maj_idsite_2()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
		UPDATE atd16_economie.geo_lot_eco AS a SET idsite = NULL WHERE st_intersects(OLD.the_geom, a.the_geom);
		UPDATE atd16_economie.geo_destination_eco AS b SET idsite = NULL WHERE st_intersects(OLD.the_geom, b.the_geom);
		UPDATE atd16_economie.geo_etablissement_eco AS c SET idsite = NULL WHERE st_intersects(OLD.the_geom, c.the_geom);
        -- Mise à jour du champ idsite avec la valeur NULL lorsque l'objet est impacté (intersecté) par la suppression d'un site
    ELSIF (TG_OP = 'INSERT') -- Si création d'un site
    OR 
    (
        (TG_OP = 'UPDATE') 
        AND ((SELECT ST_Equals(NEW.the_geom,OLD.the_geom)) IS FALSE)
    )
    -- Ou si modification géométrique d'un site
    THEN
		UPDATE atd16_economie.geo_lot_eco AS a SET idsite = NEW.gid WHERE st_intersects(NEW.the_geom, a.the_geom);
        UPDATE atd16_economie.geo_lot_eco AS a SET idsite = NULL WHERE (NEW.gid = a.idsite) AND NOT st_intersects(NEW.the_geom, a.the_geom);
		UPDATE atd16_economie.geo_destination_eco AS b SET idsite = NEW.gid WHERE st_intersects(NEW.the_geom, b.the_geom);
		UPDATE atd16_economie.geo_destination_eco AS b SET idsite = NULL WHERE (NEW.gid = b.idsite) AND NOT st_intersects(NEW.the_geom, b.the_geom);
		UPDATE atd16_economie.geo_etablissement_eco AS c SET idsite = NEW.gid WHERE st_intersects(NEW.the_geom, c.the_geom);
		UPDATE atd16_economie.geo_etablissement_eco AS c SET idsite = NULL WHERE (NEW.gid = c.idsite) AND NOT st_intersects(NEW.the_geom, c.the_geom);
        -- Mise à jour du champ idsite avec la valeur gid (site éco) lorsque l'objet est intersecté ou la valeur NULL lorsque l'objet n'est plus intersecté
        -- par la création ou la modification géographique d'un site
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_economie.f_maj_idsite_2() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_idsite_2() IS 
	'[ATD16] Mise à jour du champ idsite lorsqu''un site est créé, modifié ou supprimé';

-- #################################################################### Trigger #####################################################################

-- DROP TRIGGER "40_t_after_iud_maj_idsite_2" ON atd16_economie.geo_site_eco;

CREATE TRIGGER "40_t_after_iud_maj_idsite_2"
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_economie.geo_site_eco    
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_idsite_2();
    
COMMENT ON TRIGGER "40_t_after_iud_maj_idsite_2" ON atd16_economie.geo_site_eco IS 
    '[ATD16] Mise à jour du champ idsite lorsqu''un site est créée, modifiée ou supprimée';


-- ##################################################################################################################################################
-- ###                                      Mise à jour du nombre de lots (unités foncières) dans geo_site                                        ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_economie.f_maj_nb_unite_fonciere_2();

CREATE OR REPLACE FUNCTION atd16_economie.f_maj_nb_unite_fonciere_2()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    NEW.nb_unite_fonciere = (SELECT COUNT(b.gid) FROM atd16_economie.geo_lot_eco b WHERE ST_Intersects(b.the_geom,NEW.the_geom));
	--Le champ nb_unite_fonciere prend la valeur de la somme des lots étant intersectés avec le site créé ou modifié
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_economie.f_maj_nb_unite_fonciere_2() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_nb_unite_fonciere_2() IS 
    '[ATD16] Mise à jour du nombre d''unités foncières (lots) dans geo_site_eco lors de la création ou la modification d''un site';

-- #################################################################### Trigger #####################################################################

-- DROP TRIGGER "30_t_before_iu_maj_nb_unite_fonciere_2" ON atd16_economie.geo_site_eco;

CREATE TRIGGER "30_t_before_iu_maj_nb_unite_fonciere_2" 
    BEFORE INSERT OR UPDATE
    ON atd16_economie.geo_site_eco 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_nb_unite_fonciere_2();
    
COMMENT ON TRIGGER "30_t_before_iu_maj_nb_unite_fonciere_2" ON atd16_economie.geo_site_eco IS 
    '[ATD16] Mise à jour du nombre d''unités foncières (lots) dans geo_site_eco lors de la création ou la modification d''un site';


-- ##################################################################################################################################################
-- ###                                         Mise à jour du nombre d'établissements dans geo_site_eco                                           ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_economie.f_maj_nb_etablissement_2();

CREATE OR REPLACE FUNCTION atd16_economie.f_maj_nb_etablissement_2()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    NEW.nb_etablissement = (SELECT COUNT(b.gid) FROM atd16_economie.geo_etablissement_eco b WHERE ST_Intersects(b.the_geom,NEW.the_geom)); 
	--Le champ nb_etablissement prend la valeur de la somme des établissements étant intersectés avec le site créé ou modifié
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_economie.f_maj_nb_etablissement_2() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_nb_etablissement_2() IS 
    '[ATD16] Mise à jour du nombre d''établissements dans geo_site_eco lorsqu''un site est modifié ou créé';

-- #################################################################### Trigger #####################################################################

-- DROP TRIGGER "20_t_before_iu_maj_nb_etablissement_2" ON atd16_economie.geo_site_eco;

CREATE TRIGGER "20_t_before_iu_maj_nb_etablissement_2" 
    BEFORE INSERT OR UPDATE
    ON atd16_economie.geo_site_eco 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_nb_etablissement_2();
    
COMMENT ON TRIGGER "20_t_before_iu_maj_nb_etablissement_2" ON atd16_economie.geo_site_eco IS 
    '[ATD16] Mise à jour du nombre d''établissements dans geo_site_eco lorsqu''un site est modifié ou créé';


