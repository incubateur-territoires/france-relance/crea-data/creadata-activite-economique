-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/09/28 : RL / Création du fichier sur Git


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                 Table non géographique : Liste des types d'occupation des locaux commerciaux                               ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_economie.lst_statut_local_com;

CREATE TABLE atd16_economie.lst_statut_local_com
(
  gid serial NOT NULL, --clé primaire gid
  code varchar(2), --code, lien avec la table commerce
  libelle varchar(254), --type de la domanialité
  CONSTRAINT pk_lst_statut_local_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE atd16_economie.lst_statut_local_com
  OWNER TO sditecgrp;    

-- ################################################################## Commentaires ##################################################################




-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_economie.lst_statut_local_com (code, libelle)
	VALUES
('01', 'Vacant'),
('02', 'Occupé'),
('99', 'Inconnu');

