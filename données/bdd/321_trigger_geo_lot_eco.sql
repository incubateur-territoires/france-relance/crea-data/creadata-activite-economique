-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/10/06 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_maj_idlot_2() et du trigger associé 50_t_after_iud_maj_idlot_2
-- 2022/10/18 : SL / Ajout de la fonction f_maj_sup_foncier_dispo() et du trigger associé 70_t_after_iud_maj_sup_foncier_dispo
-- 2022/10/20 : SL / Ajout de la fonction f_maj_nb_unite_fonciere() et du trigger associé 60_t_after_iud_maj_nb_unite_fonciere
--                 . Correction de la fonction f_maj_sup_foncier_dispo()
--                 . Ajout de la fonction f_supp_valeur_idlot() et du trigger associé t_after_d_lot_supp_idlot
-- 2022/11/04 : SL / Correction de la fonction f_maj_idlot_2() : maj idlot si UPDATE géographique
-- 2022/11/09 : SL / Suppression de la fonction f_supp_valeur_idlot() et du trigger associé t_after_d_lot_supp_idlot
--                 . Ajout de la fonction f_maj_occupation_2() et du trigger associé 40_t_before_iu_maj_occupation_2

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                      Fonctions triggers et triggers spécifiques à la table geo_lot_eco                                     ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                    Mise à jour du lien idlot lorsqu'un lot est créé, modifié ou supprimé                                   ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_economie.f_maj_idlot_2();

CREATE OR REPLACE FUNCTION atd16_economie.f_maj_idlot_2()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
		UPDATE atd16_economie.geo_etablissement_eco AS c SET idlot = NULL WHERE st_intersects(OLD.the_geom, c.the_geom);
        -- Mise à jour du champ idlot avec la valeur NULL lorsque l'objet est impacté (intersecté) par la suppression d'un lot
    ELSIF (TG_OP = 'INSERT') -- Si création d'un lot
    OR 
    (
        (TG_OP = 'UPDATE') 
        AND ((SELECT ST_Equals(NEW.the_geom,OLD.the_geom)) IS FALSE)
    )
    THEN
		UPDATE atd16_economie.geo_etablissement_eco AS c SET idlot = NEW.gid WHERE st_intersects(NEW.the_geom, c.the_geom);
        UPDATE atd16_economie.geo_etablissement_eco AS c SET idlot = NULL WHERE (NEW.gid = c.idlot) AND NOT st_intersects(NEW.the_geom, c.the_geom);
        -- Mise à jour du champ idlot avec la valeur gid (lot éco) lorsque l'objet est intersecté ou NULL lorsqu'il n'est plus intersecté 
        -- par la création ou la modification d'un lot
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_economie.f_maj_idlot_2() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_idlot_2() IS 
	'[ATD16] Mise à jour du champ idlot lorsqu''un lot est créé, modifié ou supprimé';

-- #################################################################### Trigger #####################################################################

-- DROP TRIGGER "50_t_after_iud_maj_idlot_2" ON atd16_economie.geo_lot_eco;

CREATE TRIGGER "50_t_after_iud_maj_idlot_2"
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_economie.geo_lot_eco    
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_idlot_2();
    
COMMENT ON TRIGGER "50_t_after_iud_maj_idlot_2" ON atd16_economie.geo_lot_eco IS 
    '[ATD16] Mise à jour du champ idlot lorsqu''un lot est créée, modifiée ou supprimée';


-- ##################################################################################################################################################
-- ###                   Mise à jour du champ sup_foncier_dispo de la table geo_site lorsqu'un lot est créé, modifié ou supprimé                  ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_economie.f_maj_sup_foncier_dispo();

CREATE OR REPLACE FUNCTION atd16_economie.f_maj_sup_foncier_dispo()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
		UPDATE atd16_economie.geo_site_eco AS s -- Mise à jour de la table geo_site_eco
        SET sup_foncier_dispo = -- Où le champ sup_foncier_dispo prend la valeur
            (
                SELECT SUM(l.sup_m2) -- De la somme des surfaces des objets
                FROM atd16_economie.geo_lot_eco l -- De la table geo_lot_eco
                WHERE l.disponibilite = '01' -- Qui ont une disponibilité "Non bâti"
                AND l.idsite = OLD.idsite -- et un idsite identique avec celui qui est supprimé
            )
        WHERE gid::text = OLD.idsite::text; -- Où l'ancien idsite est égal au gid de la table geo_site_eco
    ELSE
		UPDATE atd16_economie.geo_site_eco AS s -- Mise à jour de la table geo_site_eco
        SET sup_foncier_dispo = -- Où le champ sup_foncier_dispo prend la valeur
            (
                SELECT SUM(l.sup_m2) -- De la somme des surfaces des objets
                FROM atd16_economie.geo_lot_eco l -- De la table geo_lot_eco
                WHERE l.disponibilite = '01' -- Qui ont une disponibilité "Non bâti"
                AND l.idsite = NEW.idsite -- et un idsite identique avec celui qui est créé ou modifié
            )
        WHERE gid::text = NEW.idsite::text; -- Où le nouvel idsite est égal au gid de la table geo_site_eco
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_economie.f_maj_sup_foncier_dispo() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_sup_foncier_dispo() IS 
	'[ATD16] Mise à jour du champ sup_foncier_dispo de la table geo_site lorsqu''un lot est créé, modifié ou supprimé';

-- #################################################################### Trigger #####################################################################

-- DROP TRIGGER "70_t_after_iud_maj_sup_foncier_dispo" ON atd16_economie.geo_lot_eco;

CREATE TRIGGER "70_t_after_iud_maj_sup_foncier_dispo"
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_economie.geo_lot_eco    
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_sup_foncier_dispo();
    
COMMENT ON TRIGGER "70_t_after_iud_maj_sup_foncier_dispo" ON atd16_economie.geo_lot_eco IS 
    '[ATD16] Mise à jour du champ sup_foncier_dispo lorsqu''un lot est créée, modifiée ou supprimée';


-- ##################################################################################################################################################
-- ###                                      Mise à jour du nombre de lots (unités foncières) dans geo_site                                        ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_economie.f_maj_nb_unite_fonciere();

CREATE OR REPLACE FUNCTION atd16_economie.f_maj_nb_unite_fonciere()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE atd16_economie.geo_site_eco -- On met à jour la table geo_site_eco
	    SET nb_unite_fonciere = --Le champ nb_unite_fonciere prend la valeur
	    (SELECT COUNT(b.gid) FROM atd16_economie.geo_lot_eco b WHERE b.idsite = OLD.idsite) 
	    --de la somme des lots ayant un idsite identique à celui qui est supprimé
	    WHERE gid::text = OLD.idsite::text; -- Où l'ancien idsite est égal au gid de la table geo_site_eco
    ELSE
        UPDATE atd16_economie.geo_site_eco -- On met à jour la table geo_site_eco
	    SET nb_unite_fonciere = --Le champ nb_unite_fonciere prend la valeur
	    (SELECT COUNT(b.gid) FROM atd16_economie.geo_lot_eco b WHERE b.idsite = NEW.idsite) 
	    --de la somme des lots ayant un idsite identique à celui qui est créé
	    WHERE gid::text = NEW.idsite::text; -- Où le nouvel idsite est égal au gid de la table geo_site_eco
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_economie.f_maj_nb_unite_fonciere() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_nb_unite_fonciere() IS 
    '[ATD16] Mise à jour du nombre d''unités foncières (lots) dans geo_site_eco lors de la suppression, modification ou création d''un lot';

-- #################################################################### Trigger #####################################################################

-- DROP TRIGGER "60_t_after_iud_maj_nb_unite_fonciere" ON atd16_economie.geo_lot_eco;

CREATE TRIGGER "60_t_after_iud_maj_nb_unite_fonciere" 
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_economie.geo_lot_eco 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_nb_unite_fonciere();
    
COMMENT ON TRIGGER "60_t_after_iud_maj_nb_unite_fonciere" ON atd16_economie.geo_lot_eco IS 
    '[ATD16] Mise à jour du nombre d''unités foncières (lots) dans geo_site_eco lors de la suppression, modification ou création d''un lot';


-- ##################################################################################################################################################
-- ###                                      Mise à jour du champ occupation lorsqu'un lot est créé ou modifié                                     ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_economie.f_maj_occupation_2();

CREATE OR REPLACE FUNCTION atd16_economie.f_maj_occupation_2()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF 
    (
        SELECT COUNT(*) FROM -- Calcul du nombre de ligne dans la table "etablot"
        (
            SELECT DISTINCT ST_Intersects(NEW.the_geom, etab.the_geom) FROM atd16_economie.geo_etablissement_eco etab
            -- Résultat des intersections entre le nouvel objet (lot) et les établissements. 
            -- Sans le DISTINCT, il y a autant de lignes que d'établissements.
            -- Le DISTINCT permet de regrouper les mêmes valeurs (2 valeurs possibles : TRUE ou FALSE)
        ) AS etablot -- nom de la table temporaire créé (indispensable)
    ) = 2 -- Si le nombre de ligne est égal à 2 (valeurs = false et true)
    THEN
		    NEW.occupation = TRUE;
            -- Mise à jour du champ occupation avec la valeur TRUE lorsque l'objet est intersecté avec un établissement
    ELSIF 
    (
        SELECT DISTINCT ST_Intersects(NEW.the_geom, etab.the_geom) FROM atd16_economie.geo_etablissement_eco etab
        -- Résultat des intersections entre le nouvel objet (lot) et les établissements. 
        -- Sans le DISTINCT, il y a autant de lignes que d'établissements.
        -- Le DISTINCT permet de regrouper les mêmes valeurs (2 valeurs possibles : TRUE ou FALSE)
    ) = TRUE -- Si la seule valeur est TRUE
    THEN
            NEW.occupation = TRUE;
            -- Mise à jour du champ occupation avec la valeur TRUE lorsque l'objet est intersecté avec un établissement
    ELSE
		    NEW.occupation = NULL;
            -- Mise à jour du champ occupation avec la valeur NULL lorsque l'objet n'est pas intersecté avec un établissement
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_economie.f_maj_occupation_2() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_occupation_2() IS 
	'[ATD16] Mise à jour du champ occupation lorsqu''un lot est créé ou modifié';

-- #################################################################### Trigger #####################################################################

-- DROP TRIGGER "40_t_before_iu_maj_occupation_2" ON atd16_economie.geo_lot_eco;

CREATE TRIGGER "40_t_before_iu_maj_occupation_2" 
    BEFORE INSERT OR UPDATE
    ON atd16_economie.geo_lot_eco 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_occupation_2();
    
COMMENT ON TRIGGER "40_t_before_iu_maj_occupation_2" ON atd16_economie.geo_lot_eco IS 
    '[ATD16] Mise à jour du champ occupation lors de la modification ou création d''un lot';


