-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/09/29 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                             Table non géographique : Domaine de valeur du type de la destination économique                                ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_economie.lst_type_destination

CREATE TABLE atd16_economie.lst_type_destination 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code du domaine de valeur
    libelle varchar(254), --[ATD16] Libellé du domaine de valeur
    CONSTRAINT pk_lst_type_destination PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_economie.lst_type_destination OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_economie.lst_type_destination TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_economie.lst_type_destination IS '[ATD16] Domaine de valeur du type de la destination économique';

COMMENT ON COLUMN atd16_economie.lst_type_destination.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_economie.lst_type_destination.code IS '[ATD16] Code du domaine de valeur';
COMMENT ON COLUMN atd16_economie.lst_type_destination.libelle IS '[ATD16] Libellé du domaine de valeur';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_economie.lst_type_destination 
    (code, libelle)
VALUES
    ('00', 'Non renseigné'),
    ('01', 'Entreprise'),
    ('02', 'Équipement'),
    ('03', 'Équipement public'),
    ('04', 'Réserve foncière'),
    ('05', 'Réserve foncière entreprise'),
    ('06', 'Réserve foncière publique'),
    ('07', 'Foncier disponible'),
    ('08', 'Friche');

