-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/09/28 : RL / Création du fichier sur Git
-- 2022/10/20 : SL / Correction du trigger update_customer_modtime_created_dossier
--                 . Correction du trigger update_customer_modtime_modified_dossier

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                    Table géographique : Commerces                                                                                          ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_economie.geo_commerce

CREATE TABLE atd16_economie.geo_commerce
(
  gid serial NOT NULL, --clé primaire
  insee varchar(20), --code insee 6 chiffres
  ident varchar(40), --nom de l'établissement
  origdata varchar(254), --origine de la donnée
  datesig date, --date d'intégration
  adresse varchar(254), --adresse du commerce
  surface_com real, --surface commerciale du local
  id_bati_ign varchar(254), --id bati (bd topo)
  ref_cada varchar (254), --référence cadastrale (pci vecteur)
  statut_local_com varchar(2), --statut local commercial
  date_statut date, --date constat statut  
  activite varchar (254), --activité (liste déroulante)
  obs_activite varchar (254), --observations sur l'activité
  date_ouverture date, --date d'ouverture du commerce
  date_fermeture date, --date fermeture du commerce
  date_creation date,
  date_maj date,
  the_geom geometry, --géométrie de l'objet
  CONSTRAINT pk_geo_commerce PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE atd16_economie.geo_commerce
  OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_economie.geo_commerce TO sditecgrp;


-- ################################################################## Commentaires ##################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

/*TRIGGER - Récupération des coordonnées géographiques*/

CREATE TRIGGER setXY_coord_trigger 
    BEFORE UPDATE OR INSERT
    ON atd16_economie.geo_commerce
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.setXY_coord();
	
/*TRIGGER - Récupération des infos cadastrales */ 

CREATE TRIGGER maj_refcad_commerce
  BEFORE INSERT OR UPDATE
  ON atd16_economie.geo_commerce
  FOR EACH ROW
  EXECUTE PROCEDURE atd16_economie.maj_refcad_commerce();  

  
/*TRIGGER - Récupération des infos du batiment */ 

CREATE TRIGGER maj_id_bati_ign
  BEFORE INSERT OR UPDATE
  ON atd16_economie.geo_commerce
  FOR EACH ROW
  EXECUTE PROCEDURE atd16_economie.maj_id_bati_ign();  
  	
	
/*TRIGGER - Définition de la date de création*/

CREATE TRIGGER update_customer_modtime_created_dossier
	BEFORE INSERT
	ON atd16_economie.geo_commerce
	FOR EACH ROW
	EXECUTE PROCEDURE atd16_economie.f_date_creation();

/*TRIGGER - Définition de la date de mise à jour*/

CREATE TRIGGER update_customer_modtime_modified_dossier
	BEFORE UPDATE
	ON atd16_economie.geo_commerce
	FOR EACH ROW
	EXECUTE PROCEDURE atd16_economie.f_date_maj();	


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE OR REPLACE VIEW atd16_economie.v_commerce AS 
 SELECT a.gid,
    a.the_geom,
    a.origdata,
    a.datesig,
    a.insee,
    a.ident,
	a.adresse,
    a.surface_com,
	a.activite,
	a.obs_activite,
	a.date_ouverture,
	a.date_fermeture,	
    a.id_bati_ign,	
    a.ref_cada,
    a.statut_local_com,
    a.date_statut,
    b.libelle AS statut_local_com_libelle
   FROM atd16_economie.geo_commerce a
     LEFT JOIN atd16_economie.lst_statut_local_com b ON a.statut_local_com::text = b.code::text;

ALTER TABLE atd16_economie.v_commerce
  OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_economie.v_commerce TO simapdatagrp;
GRANT ALL ON TABLE atd16_economie.v_commerce TO sditecgrp;
