-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/09/28 : RL / Création du fichier du schéma Economie


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                    Création du schéma : Economie                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- SCHEMA: atd16_economie

-- DROP SCHEMA atd16_economie ;

CREATE SCHEMA atd16_economie
  AUTHORIZATION sditecgrp;

COMMENT ON SCHEMA atd16_economie
    IS 'Gestion de l''activité économique';

GRANT ALL ON SCHEMA atd16_economie TO sditecgrp;


ALTER DEFAULT PRIVILEGES IN SCHEMA atd16_economie
    GRANT INSERT, SELECT, UPDATE, DELETE, TRUNCATE, REFERENCES, TRIGGER ON TABLES
    TO sditecgrp;

ALTER DEFAULT PRIVILEGES IN SCHEMA atd16_economie
    GRANT SELECT, UPDATE, USAGE ON SEQUENCES
    TO sditecgrp;

ALTER DEFAULT PRIVILEGES IN SCHEMA atd16_economie
    GRANT EXECUTE ON FUNCTIONS
    TO sditecgrp;

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/09/28 : RL / Création du fichier sur Git
--                 . Ajout de la fonction-trigger update_created_column()
--                 . Ajout de la fonction-trigger update_modified_column()
--                 . Ajout de la fonction-trigger maj_refcad_commerce()
--                 . Ajout de la fonction-trigger setXY_coord()
--                 . Ajout de la fonction-trigger maj_id_bati_ign()
-- 2022/09/29 : SL / Ajout de la fonction-trigger f_maj_sup_m2()
-- 2022/10/20 : SL / Modification de la fonction-trigger update_created_column() en f_date_creation()
--                 . Modification de la fonction-trigger update_modified_column() en f_date_maj()
--                 . Modification de la fonction-trigger setXY_coord() en f_maj_xy_coord()
--                 . Ajout de la fonction-trigger f_recup_nom_1er_proprio()
-- 2022/11/07 : SL / Correction de la fonction-trigger f_maj_sup_m2()
-- 2022/11/30 : SL / Correction de la fonction-trigger f_maj_sup_m2()
-- 2022/12/07 : SL / Correction de la fonction-trigger maj_id_bati_ign()

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                     Fonction(s) trigger(s) générique(s)                                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_economie.f_date_creation();

CREATE FUNCTION atd16_economie.f_date_creation()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    NEW.date_creation = now(); -- À la création d'un objet, le champ date_creation prend la valeur de la date du jour
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_economie.f_date_creation() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_date_creation() IS 'Fonction trigger permettant l''initialisation du champ date_creation';


-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_economie.f_date_maj();

CREATE FUNCTION atd16_economie.f_date_maj()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    NEW.date_maj = now(); -- À la modification d'un objet, le champ date_maj prend la valeur de la date du jour
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_economie.f_date_maj() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_date_maj() IS 'Fonction trigger permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                 Récupération des reférences parcellaires                                                   ###
-- ##################################################################################################################################################

-- DROP FUNCTION IF EXISTS atd16_economie.maj_refcad_commerce();

CREATE OR REPLACE FUNCTION atd16_economie.maj_refcad_commerce()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
 

BEGIN 
    
    select parc.ident 
	from pci.v_geo_parcelle_encours as parc 
	where ST_Intersects(ST_SetSRID(NEW.the_geom,0), parc.the_geom) 
	-- Le ST_SetSRID à 0 est nécessaire dans Postgis 3. En effet les nouveaux objets arrivent en 4326.
	into NEW.ref_cada;
    return NEW;

END;
$BODY$;

ALTER FUNCTION atd16_economie.maj_refcad_commerce()
    OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.maj_refcad_commerce() IS 'Fonction trigger permettant la récupération de la référence cadastrale de la parcelle';    


-- ##################################################################################################################################################
-- ###                                                 Récupération des coordonnées géographiques                                                 ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_economie.f_maj_xy_coord();

CREATE OR REPLACE FUNCTION atd16_economie.f_maj_xy_coord()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables (inutile ici)
    
BEGIN
    NEW.x_l93 := ST_X(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154)); -- On récupère le X, on le définit en 3857, puis on le met en 2154
    NEW.y_l93 := ST_Y(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154)); -- On récupère le Y, on le définit en 3857, puis on le met en 2154
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_economie.f_maj_xy_coord() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_xy_coord() IS 
    'Fonction trigger permettant la récupération des coordonnées géographiques';


-- ##################################################################################################################################################
-- ###                                                 Récupération de l'ID batiment de la BD Topo                                                 ###
-- ##################################################################################################################################################
	
-- DROP FUNCTION atd16_economie.maj_id_bati_ign();
	
CREATE OR REPLACE FUNCTION atd16_economie.maj_id_bati_ign()
  RETURNS trigger AS
$BODY$ 

BEGIN 
    
    select bati.cleabs 
	from ign_bdtopo.geo_batiment as bati 
	where ST_Intersects(ST_SetSRID(NEW.the_geom,3857), bati.the_geom) 
	-- Le ST_SetSRID à 0 est nécessaire dans Postgis 3. En effet les nouveaux objets arrivent en 4326. (obsolète raison inconnu, cela marche en 3857)
	into NEW.id_bati_ign;
    return NEW;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION atd16_economie.maj_id_bati_ign()
  OWNER TO sditecgrp;


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ sup_m2                                                         ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_economie.f_maj_sup_m2();

CREATE FUNCTION atd16_economie.f_maj_sup_m2()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    IF NEW.sup_m2 IS NULL -- Si sup_m2 est null
    THEN --Alors
        NEW.sup_m2 = ST_Area(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154)); -- Calcul de la surface à partir de la géométrie 
        -- reprojection car en 3857 la surface est à peu près deux fois plus grande
    END IF; 
	-- Il n'y a pas de modification lorsqu'une valeur (autre que NULL) est ajouté dans le champ sup_m2
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_espaces_verts.f_maj_sup_m2() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_espaces_verts.f_maj_sup_m2() IS 'Fonction trigger permettant la mise à jour du champ sup_m2';


-- ##################################################################################################################################################
-- ###                                                   Récupération du premier propriétaire                                                     ###
-- ##################################################################################################################################################

-- DROP FUNCTION IF EXISTS atd16_economie.f_recup_nom_1er_proprio();

CREATE OR REPLACE FUNCTION atd16_economie.f_recup_nom_1er_proprio()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
 
BEGIN 
    
    SELECT parc.nom 
	FROM pci.v_geo_parcelle_encours as parc 
	WHERE ST_Intersects(ST_SetSRID(NEW.the_geom,0), parc.the_geom) 
	-- Le ST_SetSRID à 0 est nécessaire dans Postgis 3. En effet les nouveaux objets arrivent en 4326.
	INTO NEW.proprietaire;
    RETURN NEW;

END;
$BODY$;

ALTER FUNCTION atd16_economie.f_recup_nom_1er_proprio() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_recup_nom_1er_proprio() IS 
    'Fonction trigger permettant la récupération du premier propriétaire de la parcelle cadastrale';    


-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/09/28 : RL / Création du fichier sur Git


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                 Table non géographique : Liste des types d'occupation des locaux commerciaux                               ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_economie.lst_statut_local_com;

CREATE TABLE atd16_economie.lst_statut_local_com
(
  gid serial NOT NULL, --clé primaire gid
  code varchar(2), --code, lien avec la table commerce
  libelle varchar(254), --type de la domanialité
  CONSTRAINT pk_lst_statut_local_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE atd16_economie.lst_statut_local_com
  OWNER TO sditecgrp;    

-- ################################################################## Commentaires ##################################################################




-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_economie.lst_statut_local_com (code, libelle)
	VALUES
('01', 'Vacant'),
('02', 'Occupé'),
('99', 'Inconnu');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/09/28 : RL / Création du fichier sur Git
-- 2022/10/20 : SL / Correction du trigger update_customer_modtime_created_dossier
--                 . Correction du trigger update_customer_modtime_modified_dossier

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                    Table géographique : Commerces                                                                                          ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_economie.geo_commerce

CREATE TABLE atd16_economie.geo_commerce
(
  gid serial NOT NULL, --clé primaire
  insee varchar(20), --code insee 6 chiffres
  ident varchar(40), --nom de l'établissement
  origdata varchar(254), --origine de la donnée
  datesig date, --date d'intégration
  adresse varchar(254), --adresse du commerce
  surface_com real, --surface commerciale du local
  id_bati_ign varchar(254), --id bati (bd topo)
  ref_cada varchar (254), --référence cadastrale (pci vecteur)
  statut_local_com varchar(2), --statut local commercial
  date_statut date, --date constat statut  
  activite varchar (254), --activité (liste déroulante)
  obs_activite varchar (254), --observations sur l'activité
  date_ouverture date, --date d'ouverture du commerce
  date_fermeture date, --date fermeture du commerce
  date_creation date,
  date_maj date,
  the_geom geometry, --géométrie de l'objet
  CONSTRAINT pk_geo_commerce PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE atd16_economie.geo_commerce
  OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_economie.geo_commerce TO sditecgrp;


-- ################################################################## Commentaires ##################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

/*TRIGGER - Récupération des coordonnées géographiques*/

CREATE TRIGGER setXY_coord_trigger 
    BEFORE UPDATE OR INSERT
    ON atd16_economie.geo_commerce
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.setXY_coord();
	
/*TRIGGER - Récupération des infos cadastrales */ 

CREATE TRIGGER maj_refcad_commerce
  BEFORE INSERT OR UPDATE
  ON atd16_economie.geo_commerce
  FOR EACH ROW
  EXECUTE PROCEDURE atd16_economie.maj_refcad_commerce();  

  
/*TRIGGER - Récupération des infos du batiment */ 

CREATE TRIGGER maj_id_bati_ign
  BEFORE INSERT OR UPDATE
  ON atd16_economie.geo_commerce
  FOR EACH ROW
  EXECUTE PROCEDURE atd16_economie.maj_id_bati_ign();  
  	
	
/*TRIGGER - Définition de la date de création*/

CREATE TRIGGER update_customer_modtime_created_dossier
	BEFORE INSERT
	ON atd16_economie.geo_commerce
	FOR EACH ROW
	EXECUTE PROCEDURE atd16_economie.f_date_creation();

/*TRIGGER - Définition de la date de mise à jour*/

CREATE TRIGGER update_customer_modtime_modified_dossier
	BEFORE UPDATE
	ON atd16_economie.geo_commerce
	FOR EACH ROW
	EXECUTE PROCEDURE atd16_economie.f_date_maj();	


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE OR REPLACE VIEW atd16_economie.v_commerce AS 
 SELECT a.gid,
    a.the_geom,
    a.origdata,
    a.datesig,
    a.insee,
    a.ident,
	a.adresse,
    a.surface_com,
	a.activite,
	a.obs_activite,
	a.date_ouverture,
	a.date_fermeture,	
    a.id_bati_ign,	
    a.ref_cada,
    a.statut_local_com,
    a.date_statut,
    b.libelle AS statut_local_com_libelle
   FROM atd16_economie.geo_commerce a
     LEFT JOIN atd16_economie.lst_statut_local_com b ON a.statut_local_com::text = b.code::text;

ALTER TABLE atd16_economie.v_commerce
  OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_economie.v_commerce TO simapdatagrp;
GRANT ALL ON TABLE atd16_economie.v_commerce TO sditecgrp;
-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/09/29 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                    Table non géographique : Domaine de valeur de la vocation du site                                       ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_economie.lst_vocation_site

CREATE TABLE atd16_economie.lst_vocation_site 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code du domaine de valeur
    libelle varchar(254), --[ATD16] Libellé du domaine de valeur
    CONSTRAINT pk_lst_vocation_site PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_economie.lst_vocation_site OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_economie.lst_vocation_site TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_economie.lst_vocation_site IS '[ATD16] Domaine de valeur de la vocation du site';

COMMENT ON COLUMN atd16_economie.lst_vocation_site.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_economie.lst_vocation_site.code IS '[ATD16] Code du domaine de valeur';
COMMENT ON COLUMN atd16_economie.lst_vocation_site.libelle IS '[ATD16] Libellé du domaine de valeur';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_economie.lst_vocation_site 
    (code, libelle)
VALUES
    ('00', 'Non renseigné'),
    ('01', 'ZI - zone industrielle'),
    ('02', 'ZA - zone artisanale'),
    ('03', 'ZC - zone commerciale'),
    ('04', 'ZM - zone mixte');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/09/29 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                        Table non géographique : Domaine de valeur du type de site                                          ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_economie.lst_type_site

CREATE TABLE atd16_economie.lst_type_site 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code du domaine de valeur
    libelle varchar(254), --[ATD16] Libellé du domaine de valeur
    CONSTRAINT pk_lst_type_site PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_economie.lst_type_site OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_economie.lst_type_site TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_economie.lst_type_site IS '[ATD16] Domaine de valeur du type de site';

COMMENT ON COLUMN atd16_economie.lst_type_site.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_economie.lst_type_site.code IS '[ATD16] Code du domaine de valeur';
COMMENT ON COLUMN atd16_economie.lst_type_site.libelle IS '[ATD16] Libellé du domaine de valeur';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_economie.lst_type_site 
    (code, libelle)
VALUES
    ('00', 'Non renseigné'),
    ('01', 'ZAE'),
    ('02', 'Autre site d''activité identifié (hors ZAE)'),
    ('03', 'Autre secteur (non exclusivement économique)');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/09/29 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                        Table non géographique : Domaine de valeur de l'état du site                                          ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_economie.lst_etat_site

CREATE TABLE atd16_economie.lst_etat_site 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code du domaine de valeur
    libelle varchar(254), --[ATD16] Libellé du domaine de valeur
    CONSTRAINT pk_lst_etat_site PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_economie.lst_etat_site OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_economie.lst_etat_site TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_economie.lst_etat_site IS '[ATD16] Domaine de valeur de l''état du site';

COMMENT ON COLUMN atd16_economie.lst_etat_site.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_economie.lst_etat_site.code IS '[ATD16] Code du domaine de valeur';
COMMENT ON COLUMN atd16_economie.lst_etat_site.libelle IS '[ATD16] Libellé du domaine de valeur';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_economie.lst_etat_site 
    (code, libelle)
VALUES
    ('00', 'Non renseigné'),
    ('01', 'Existant'),
    ('02', 'Extension'),
    ('03', 'Création'),
    ('04', 'Déclassé'),
    ('05', 'Projet de déclassement');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/09/29 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                  Table non géographique : Domaine de valeur de la disponibilité du lot                                     ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_economie.lst_disponibilite_lot

CREATE TABLE atd16_economie.lst_disponibilite_lot 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code du domaine de valeur
    libelle varchar(254), --[ATD16] Libellé du domaine de valeur
    CONSTRAINT pk_lst_disponibilite_lot PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_economie.lst_disponibilite_lot OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_economie.lst_disponibilite_lot TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_economie.lst_disponibilite_lot IS '[ATD16] Domaine de valeur de la disponibilité du lot';

COMMENT ON COLUMN atd16_economie.lst_disponibilite_lot.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_economie.lst_disponibilite_lot.code IS '[ATD16] Code du domaine de valeur';
COMMENT ON COLUMN atd16_economie.lst_disponibilite_lot.libelle IS '[ATD16] Libellé du domaine de valeur';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_economie.lst_disponibilite_lot 
    (code, libelle)
VALUES
    ('00', 'Non renseigné'),
    ('01', 'Non bâti'),
    ('02', 'Bâti vacant'),
    ('03', 'Occupé, sous optimisé ou potentiellement divisable'),
    ('04', 'Occupé et optimisé');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/09/29 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                             Table non géographique : Domaine de valeur du type de la destination économique                                ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_economie.lst_type_destination

CREATE TABLE atd16_economie.lst_type_destination 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code du domaine de valeur
    libelle varchar(254), --[ATD16] Libellé du domaine de valeur
    CONSTRAINT pk_lst_type_destination PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_economie.lst_type_destination OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_economie.lst_type_destination TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_economie.lst_type_destination IS '[ATD16] Domaine de valeur du type de la destination économique';

COMMENT ON COLUMN atd16_economie.lst_type_destination.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_economie.lst_type_destination.code IS '[ATD16] Code du domaine de valeur';
COMMENT ON COLUMN atd16_economie.lst_type_destination.libelle IS '[ATD16] Libellé du domaine de valeur';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO atd16_economie.lst_type_destination 
    (code, libelle)
VALUES
    ('00', 'Non renseigné'),
    ('01', 'Entreprise'),
    ('02', 'Équipement'),
    ('03', 'Équipement public'),
    ('04', 'Réserve foncière'),
    ('05', 'Réserve foncière entreprise'),
    ('06', 'Réserve foncière publique'),
    ('07', 'Foncier disponible'),
    ('08', 'Friche');

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/09/29 : SL / Création du fichier sur Git
--                 . Ajout du trigger 00_t_before_i_init_date_creation
--                 . Ajout du trigger 02_t_before_u_date_maj
--                 . Ajout du trigger 10_t_before_iu_maj_sup_m2
-- 2022/10/20 : SL / Correction du trigger 02_t_before_u_date_maj
--                 . Ajout du champ nb_unite_fonciere
--                 . Ajout du champ taux_vacance
-- 2022/11/03 : SL / Correction du champ taux_vacance en numeric(5,2)
-- 2022/11/09 : SL / Suppression du champ taux_vacance
--                 . Ajout de la vue v_geo_site_eco avec calcul du taux_vacance
-- 2022/12/05 : SL / Ajout du champ observation

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                  Table géographique : Sites économiques / Zones d'Activité Économiques                                     ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_economie.geo_site_eco;

CREATE TABLE atd16_economie.geo_site_eco
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    ident varchar(254), --[SIRAP] Libellé du site - Identifiant SIRAP
    vocation_site varchar(2), --[FK][ATD16] Identifiant de la vocation du site
    type_site varchar(2), --[FK][ATD16] Identifiant du type du site
    etat_site varchar(2), --[FK][ATD16] Identifiant de l'état du site
    sup_m2 integer, --[ATD16] Superficie du site en m² (semi-automatique)
    sup_foncier_dispo integer, --[ATD16] Surface foncière disponible sur le site en m² (automatique)
    nb_unite_fonciere integer, --[ATD16] Nombre d'unités foncières (lots) sur le site (automatique)
    nb_etablissement integer, --[ATD16] Nombre d'établissements (occupants) sur le site (automatique)
    annee_creation integer, --[ATD16] Année de création du site
    observation varchar(254), --[ATD16] Observations sur la zone
    date_creation timestamp, --[ATD16] Date de création de l'objet (automatique)
    date_maj timestamp, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_site_eco PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_economie.geo_site_eco OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_economie.geo_site_eco TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_economie.geo_site_eco IS '[ATD16] Table contenant la géographie des sites économiques (ZAE)';

COMMENT ON COLUMN atd16_economie.geo_site_eco.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_economie.geo_site_eco.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_economie.geo_site_eco.ident IS '[SIRAP] Libellé du site - Identifiant SIRAP';
COMMENT ON COLUMN atd16_economie.geo_site_eco.vocation_site IS '[FK][ATD16] Identifiant de la vocation du site';
COMMENT ON COLUMN atd16_economie.geo_site_eco.type_site IS '[FK][ATD16] Identifiant du type du site';
COMMENT ON COLUMN atd16_economie.geo_site_eco.etat_site IS '[FK][ATD16] Identifiant de l''état du site';
COMMENT ON COLUMN atd16_economie.geo_site_eco.sup_m2 IS '[ATD16] Superficie du site en m² (semi-automatique)';
COMMENT ON COLUMN atd16_economie.geo_site_eco.sup_foncier_dispo IS '[ATD16] Surface foncière disponible sur le site en m² (automatique)';
COMMENT ON COLUMN atd16_economie.geo_site_eco.nb_unite_fonciere IS '[ATD16] Nombre d''unités foncières (lots) sur le site (automatique)';
COMMENT ON COLUMN atd16_economie.geo_site_eco.nb_etablissement IS '[ATD16] Nombre d''établissements (occupants) sur le site (automatique)';
COMMENT ON COLUMN atd16_economie.geo_site_eco.annee_creation IS '[ATD16] Année de création du site';
COMMENT ON COLUMN atd16_economie.geo_site_eco.observation IS '[ATD16] Observations sur la zone';
COMMENT ON COLUMN atd16_economie.geo_site_eco.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_economie.geo_site_eco.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_economie.geo_site_eco.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                             v_geo_site_eco : vue des sites économiques (ZAE) avec le calcul du taux de vacance                             ###
-- ##################################################################################################################################################

-- DROP VIEW atd16_economie.v_geo_site_eco;

CREATE OR REPLACE VIEW atd16_economie.v_geo_site_eco 
    AS 
    SELECT 
        a.gid,
        a.insee,
        a.ident,
        a.vocation_site,
        a.type_site,
        a.etat_site,
        a.sup_m2,
        a.sup_foncier_dispo,
        a.nb_unite_fonciere,
        a.nb_etablissement,
        CASE 
            WHEN a.nb_unite_fonciere::numeric = 0 THEN NULL
            ELSE (((a.nb_unite_fonciere::numeric - a.nb_etablissement::numeric) / a.nb_unite_fonciere::numeric) * 100)::numeric(5,2)
        END taux_vacance,
        a.annee_creation,
        a.observation,
        a.date_creation,
        a.date_maj,
        a.the_geom
    FROM atd16_economie.geo_site_eco a;

ALTER TABLE atd16_economie.v_geo_site_eco OWNER TO sditecgrp;

COMMENT ON VIEW atd16_economie.v_geo_site_eco IS 
    '[ATD16] Vue listant les sites économiques (ZAE) avec le calcul du taux de vacance de la ZAE telle que défini dans l''article L318-8-2 du Code de l''urbanisme';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                          Trigger(s) générique(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER "00_t_before_i_init_date_creation" ON atd16_economie.geo_site_eco;

CREATE TRIGGER "00_t_before_i_init_date_creation"
    BEFORE INSERT
    ON atd16_economie.geo_site_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_date_creation();
    
COMMENT ON TRIGGER "00_t_before_i_init_date_creation" ON atd16_economie.geo_site_eco IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER "02_t_before_u_date_maj" ON atd16_economie.geo_site_eco;

CREATE TRIGGER "02_t_before_u_date_maj"
    BEFORE UPDATE
    ON atd16_economie.geo_site_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_date_maj();
    
COMMENT ON TRIGGER "02_t_before_u_date_maj" ON atd16_economie.geo_site_eco IS
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ sup_m2                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER "10_t_before_iu_maj_sup_m2" ON atd16_economie.geo_site_eco;

CREATE TRIGGER "10_t_before_iu_maj_sup_m2"
    BEFORE INSERT OR UPDATE
    ON atd16_economie.geo_site_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_sup_m2();
    
COMMENT ON TRIGGER "10_t_before_iu_maj_sup_m2" ON atd16_economie.geo_site_eco IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ sup_m2';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/10/05 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_maj_idsite()
--                 . Ajout de la fonction f_maj_taux_vacance()
-- 2022/11/03 : SL / Correction de la fonction f_maj_taux_vacance()
-- 2022/11/09 : SL / Suppression de la fonction f_maj_taux_vacance()

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                     Fonction(s) trigger(s) dépendante(s)                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###              Mise à jour du champ idsite à la création ou la modification d'un lot, d'une destination ou d'un établissement                ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_economie.f_maj_idsite();

CREATE FUNCTION atd16_economie.f_maj_idsite()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    SELECT gid -- on sélectionne le champ gid
    FROM atd16_economie.geo_site_eco AS site_eco -- de la table geo_site_eco
    WHERE ST_Intersects(NEW.the_geom, site_eco.the_geom) -- qui intersecte le nouvel objet créé
    INTO NEW.idsite; -- et on insère la valeur de gid dans le nouveau champ idsite
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_economie.f_maj_idsite() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_idsite() IS
    'Fonction trigger permettant la mise à jour du champ idsite lors de la création ou la modification d''un lot, d''une destination ou d''un établissement';


-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/09/29 : SL / Création du fichier sur Git
--                 . Ajout du trigger 00_t_before_i_init_date_creation
--                 . Ajout du trigger 02_t_before_u_date_maj
--                 . Ajout du trigger 10_t_before_iu_maj_sup_m2
-- 2022/10/05 : SL / Ajout du trigger 20_t_before_iu_maj_idsite
-- 2022/10/20 : SL / Correction du trigger 02_t_before_u_date_maj
--                 . Ajout du trigger t_after_iud_maj_taux_vacance
-- 2022/11/09 : SL / Suppression du trigger t_after_iud_maj_taux_vacance

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                               Table géographique : Lots (unités foncières)                                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_economie.geo_lot_eco;

CREATE TABLE atd16_economie.geo_lot_eco
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idsite integer, --[FK][ATD16] Identifiant du site économique
    insee varchar(6), --[ATD16] Code INSEE de la commune
    ident varchar(254), --[SIRAP] Numéro du lot - Identifiant SIRAP
    disponibilite varchar(2), --[FK][ATD16] Identifiant de la disponibilité du lot
    occupation boolean, --[ATD16] Occupation de l'unité foncière (automatique)
    sup_m2 integer, --[ATD16] Superficie du lot en m² (semi-automatique)
    sup_bati integer, --[ATD16] Surface du bâti sur le lot en m²
    proprietaire varchar(254), --[ATD16] Nom du propriétaire (automatique)
    date_creation timestamp, --[ATD16] Date de création de l'objet (automatique)
    date_maj timestamp, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_lot_eco PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_economie.geo_lot_eco OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_economie.geo_lot_eco TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_economie.geo_lot_eco IS '[ATD16] Table contenant la géographie des lots économiques ';

COMMENT ON COLUMN atd16_economie.geo_lot_eco.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_economie.geo_lot_eco.idsite IS '[FK][ATD16] Identifiant du site économique';
COMMENT ON COLUMN atd16_economie.geo_lot_eco.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_economie.geo_lot_eco.ident IS '[SIRAP] Numéro du lot - Identifiant SIRAP';
COMMENT ON COLUMN atd16_economie.geo_lot_eco.disponibilite IS '[FK][ATD16] Identifiant de la disponibilité du lot';
COMMENT ON COLUMN atd16_economie.geo_lot_eco.occupation IS '[ATD16] Occupation de l''unité foncière (automatique)';
COMMENT ON COLUMN atd16_economie.geo_lot_eco.sup_m2 IS '[ATD16] Superficie du lot en m² (semi-automatique)';
COMMENT ON COLUMN atd16_economie.geo_lot_eco.sup_bati IS '[ATD16] Surface du bâti sur le lot en m²';
COMMENT ON COLUMN atd16_economie.geo_lot_eco.proprietaire IS '[ATD16] Nom du propriétaire (automatique)';
COMMENT ON COLUMN atd16_economie.geo_lot_eco.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_economie.geo_lot_eco.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_economie.geo_lot_eco.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################



-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                          Trigger(s) générique(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER "00_t_before_i_init_date_creation" ON atd16_economie.geo_lot_eco;

CREATE TRIGGER "00_t_before_i_init_date_creation"
    BEFORE INSERT
    ON atd16_economie.geo_lot_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_date_creation();
    
COMMENT ON TRIGGER "00_t_before_i_init_date_creation" ON atd16_economie.geo_lot_eco IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER "02_t_before_u_date_maj" ON atd16_economie.geo_lot_eco;

CREATE TRIGGER "02_t_before_u_date_maj"
    BEFORE UPDATE
    ON atd16_economie.geo_lot_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_date_maj();
    
COMMENT ON TRIGGER "02_t_before_u_date_maj" ON atd16_economie.geo_lot_eco IS
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ sup_m2                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER "10_t_before_iu_maj_sup_m2" ON atd16_economie.geo_lot_eco;

CREATE TRIGGER "10_t_before_iu_maj_sup_m2"
    BEFORE INSERT OR UPDATE
    ON atd16_economie.geo_lot_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_sup_m2();
    
COMMENT ON TRIGGER "10_t_before_iu_maj_sup_m2" ON atd16_economie.geo_lot_eco IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ sup_m2';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idsite                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER "20_t_before_iu_maj_idsite" ON atd16_economie.geo_lot_eco;

CREATE TRIGGER "20_t_before_iu_maj_idsite"
    BEFORE INSERT OR UPDATE
    ON atd16_economie.geo_lot_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_idsite();
    
COMMENT ON TRIGGER "20_t_before_iu_maj_idsite" ON atd16_economie.geo_lot_eco IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idsite';


-- ##################################################################################################################################################
-- ###                                                     Mise à jour du champ proprietaire                                                      ###
-- ##################################################################################################################################################

-- DROP TRIGGER "30_t_before_iu_maj_proprietaire" ON atd16_economie.geo_lot_eco;

CREATE TRIGGER "30_t_before_iu_maj_proprietaire"
    BEFORE INSERT OR UPDATE
    ON atd16_economie.geo_lot_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_recup_nom_1er_proprio();
    
COMMENT ON TRIGGER "30_t_before_iu_maj_proprietaire" ON atd16_economie.geo_lot_eco IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ proprietaire';


-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/10/05 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_maj_idlot()

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                     Fonction(s) trigger(s) dépendante(s)                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idlot                                                         ###
-- ##################################################################################################################################################

-- DROP FUNCTION atd16_economie.f_maj_idlot();

CREATE FUNCTION atd16_economie.f_maj_idlot()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    SELECT gid -- on sélectionne le champ gid
    FROM atd16_economie.geo_lot_eco AS lot_eco -- de la table geo_lot_eco
    WHERE ST_Intersects(NEW.the_geom, lot_eco.the_geom) -- qui intersecte le nouvel objet créé
    INTO NEW.idlot; -- et on insère la valeur de gid dans le nouveau champ idlot
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION atd16_economie.f_maj_idlot() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_idlot() IS 'Fonction trigger permettant la mise à jour du champ idlot';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/10/05 : SL / Création du fichier sur Git
--                 . Ajout du trigger 00_t_before_i_init_date_creation
--                 . Ajout du trigger 02_t_before_u_date_maj
--                 . Ajout du trigger 10_t_before_iu_maj_sup_m2
--                 . Ajout du trigger 20_t_before_iu_maj_idsite
-- 2022/10/20 : SL / Correction du trigger 02_t_before_u_date_maj

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                               Table géographique : Destination économique                                                  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_economie.geo_destination_eco;

CREATE TABLE atd16_economie.geo_destination_eco
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idsite integer, --[FK][ATD16] Identifiant du site économique
    insee varchar(6), --[ATD16] Code INSEE de la commune
    ident varchar(254), --[SIRAP] Nom de la destination - Identifiant SIRAP
    type_destination varchar(2), --[FK][ATD16] Identifiant du type de destination
    sup_m2 integer, --[ATD16] Superficie de la destination en m² (semi-automatique)
    date_creation timestamp, --[ATD16] Date de création de l'objet (automatique)
    date_maj timestamp, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_destination_eco PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_economie.geo_destination_eco OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_economie.geo_destination_eco TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_economie.geo_destination_eco IS '[ATD16] Table contenant la géographie des destinations économiques ';

COMMENT ON COLUMN atd16_economie.geo_destination_eco.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_economie.geo_destination_eco.idsite IS '[FK][ATD16] Identifiant du site économique';
COMMENT ON COLUMN atd16_economie.geo_destination_eco.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_economie.geo_destination_eco.ident IS '[SIRAP] Nom de la destination - Identifiant SIRAP';
COMMENT ON COLUMN atd16_economie.geo_destination_eco.type_destination IS '[FK][ATD16] Identifiant du type de destination';
COMMENT ON COLUMN atd16_economie.geo_destination_eco.sup_m2 IS '[ATD16] Superficie de la destination en m² (semi-automatique)';
COMMENT ON COLUMN atd16_economie.geo_destination_eco.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_economie.geo_destination_eco.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_economie.geo_destination_eco.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################



-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                          Trigger(s) générique(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER "00_t_before_i_init_date_creation" ON atd16_economie.geo_destination_eco;

CREATE TRIGGER "00_t_before_i_init_date_creation"
    BEFORE INSERT
    ON atd16_economie.geo_destination_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_date_creation();
    
COMMENT ON TRIGGER "00_t_before_i_init_date_creation" ON atd16_economie.geo_destination_eco IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER "02_t_before_u_date_maj" ON atd16_economie.geo_destination_eco;

CREATE TRIGGER "02_t_before_u_date_maj"
    BEFORE UPDATE
    ON atd16_economie.geo_destination_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_date_maj();
    
COMMENT ON TRIGGER "02_t_before_u_date_maj" ON atd16_economie.geo_destination_eco IS
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ sup_m2                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER "10_t_before_iu_maj_sup_m2" ON atd16_economie.geo_destination_eco;

CREATE TRIGGER "10_t_before_iu_maj_sup_m2"
    BEFORE INSERT OR UPDATE
    ON atd16_economie.geo_destination_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_sup_m2();
    
COMMENT ON TRIGGER "10_t_before_iu_maj_sup_m2" ON atd16_economie.geo_destination_eco IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ sup_m2';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idsite                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER "20_t_before_iu_maj_idsite" ON atd16_economie.geo_destination_eco;

CREATE TRIGGER "20_t_before_iu_maj_idsite"
    BEFORE INSERT OR UPDATE
    ON atd16_economie.geo_destination_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_idsite();
    
COMMENT ON TRIGGER "20_t_before_iu_maj_idsite" ON atd16_economie.geo_destination_eco IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idsite';

-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/10/05 : SL / Création du fichier sur Git
--                 . Ajout du trigger 00_t_before_i_init_date_creation
--                 . Ajout du trigger 02_t_before_u_date_maj
--                 . Ajout du trigger 10_t_before_iu_maj_xy_coord
--                 . Ajout du trigger 20_t_before_iu_maj_idsite
--                 . Ajout du trigger 30_t_before_iu_maj_idlot
-- 2022/10/20 : SL / Correction du trigger 02_t_before_u_date_maj
--                 . Ajout du trigger t_after_iud_maj_taux_vacance
-- 2022/11/09 : SL / Suppression du trigger t_after_iud_maj_taux_vacance

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                               Table géographique : Établissement économique                                                ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_economie.geo_etablissement_eco;

CREATE TABLE atd16_economie.geo_etablissement_eco
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idsite integer, --[FK][ATD16] Identifiant du site économique
    idlot integer, --[FK][ATD16] Identifiant du lot économique
    insee varchar(6), --[ATD16] Code INSEE de la commune
    ident varchar(254), --[SIRAP] Nom de l'établissement - Identifiant SIRAP
    x_l93 numeric(10,3), --[ATD16] Coordonnées X en Lambert 93 (automatique)
    y_l93 numeric(10,3), --[ATD16] Coordonnées Y en Lambert 93 (automatique)
    date_creation timestamp, --[ATD16] Date de création de l'objet (automatique)
    date_maj timestamp, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_etablissement_eco PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_economie.geo_etablissement_eco OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_economie.geo_etablissement_eco TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_economie.geo_etablissement_eco IS '[ATD16] Table contenant la géographie des établissements économiques';

COMMENT ON COLUMN atd16_economie.geo_etablissement_eco.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_economie.geo_etablissement_eco.idsite IS '[FK][ATD16] Identifiant du site économique';
COMMENT ON COLUMN atd16_economie.geo_etablissement_eco.idlot IS '[FK][ATD16] Identifiant du lot économique';
COMMENT ON COLUMN atd16_economie.geo_etablissement_eco.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_economie.geo_etablissement_eco.ident IS '[SIRAP] Nom de l''établissement - Identifiant SIRAP';
COMMENT ON COLUMN atd16_economie.geo_etablissement_eco.x_l93 IS '[ATD16] Coordonnées X en Lambert 93 (automatique)';
COMMENT ON COLUMN atd16_economie.geo_etablissement_eco.y_l93 IS '[ATD16] Coordonnées Y en Lambert 93 (automatique)';
COMMENT ON COLUMN atd16_economie.geo_etablissement_eco.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_economie.geo_etablissement_eco.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_economie.geo_etablissement_eco.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################



-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                          Trigger(s) générique(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER "00_t_before_i_init_date_creation" ON atd16_economie.geo_etablissement_eco;

CREATE TRIGGER "00_t_before_i_init_date_creation"
    BEFORE INSERT
    ON atd16_economie.geo_etablissement_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_date_creation();
    
COMMENT ON TRIGGER "00_t_before_i_init_date_creation" ON atd16_economie.geo_etablissement_eco IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER "02_t_before_u_date_maj" ON atd16_economie.geo_etablissement_eco;

CREATE TRIGGER "02_t_before_u_date_maj"
    BEFORE UPDATE
    ON atd16_economie.geo_etablissement_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_date_maj();
    
COMMENT ON TRIGGER "02_t_before_u_date_maj" ON atd16_economie.geo_etablissement_eco IS
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                    Mise à jour des champs x_l93 et y_l93                                                   ###
-- ##################################################################################################################################################

-- DROP TRIGGER "10_t_before_iu_maj_xy_coord" ON atd16_economie.geo_etablissement_eco;

CREATE TRIGGER "10_t_before_iu_maj_xy_coord"
    BEFORE INSERT OR UPDATE
    ON atd16_economie.geo_etablissement_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_xy_coord();
    
COMMENT ON TRIGGER "10_t_before_iu_maj_xy_coord" ON atd16_economie.geo_etablissement_eco IS 
    'Trigger déclenchant la fonction permettant la mise à jour des champs x_l93 et y_l93';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idsite                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER "20_t_before_iu_maj_idsite" ON atd16_economie.geo_etablissement_eco;

CREATE TRIGGER "20_t_before_iu_maj_idsite"
    BEFORE INSERT OR UPDATE
    ON atd16_economie.geo_etablissement_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_idsite();
    
COMMENT ON TRIGGER "20_t_before_iu_maj_idsite" ON atd16_economie.geo_etablissement_eco IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idsite';


-- ##################################################################################################################################################
-- ###                                                         Mise à jour du champ idlot                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER "30_t_before_iu_maj_idlot" ON atd16_economie.geo_etablissement_eco;

CREATE TRIGGER "30_t_before_iu_maj_idlot"
    BEFORE INSERT OR UPDATE
    ON atd16_economie.geo_etablissement_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_idlot();
    
COMMENT ON TRIGGER "30_t_before_iu_maj_idlot" ON atd16_economie.geo_etablissement_eco IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idlot';


-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/10/05 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_maj_idsite_2() et du trigger associé 40_t_after_iud_maj_idsite_2
-- 2022/10/20 : SL / Ajout de la fonction f_supp_valeur_idsite() et du trigger associé t_after_d_site_supp_idsite
-- 2022/11/04 : SL / Correction de la fonction f_maj_idsite_2() : maj_idsite si UPDATE géographique
--                 . Ajout de la fonction f_maj_nb_unite_fonciere_2() et du trigger associé 30_t_after_iu_maj_nb_unite_fonciere_2
--                 . Ajout de la fonction f_maj_nb_etablissement_2() et du trigger associé 20_t_after_iu_maj_nb_etablissement_2
--                 . Ajout de la fonction f_maj_taux_vacance_2() et du trigger associé t_after_iu_maj_taux_vacance_2
-- 2022/11/07 : SL / Correction de la fonction f_maj_taux_vacance_2()
--                 . Correction de la fonction f_maj_nb_etablissement_2()
--                 . Correction de la fonction f_maj_nb_unite_fonciere_2()
-- 2022/11/09 : SL / Suppression de la fonction f_maj_taux_vacance_2()
--                 . Suppression de la fonction f_supp_valeur_idsite() et du trigger associé t_after_d_site_supp_idsite

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                      Fonctions triggers et triggers spécifiques à la table geo_site_eco                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                   Mise à jour du lien idsite lorsqu'un site est créé, modifié ou supprimé                                  ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_economie.f_maj_idsite_2();

CREATE OR REPLACE FUNCTION atd16_economie.f_maj_idsite_2()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
		UPDATE atd16_economie.geo_lot_eco AS a SET idsite = NULL WHERE st_intersects(OLD.the_geom, a.the_geom);
		UPDATE atd16_economie.geo_destination_eco AS b SET idsite = NULL WHERE st_intersects(OLD.the_geom, b.the_geom);
		UPDATE atd16_economie.geo_etablissement_eco AS c SET idsite = NULL WHERE st_intersects(OLD.the_geom, c.the_geom);
        -- Mise à jour du champ idsite avec la valeur NULL lorsque l'objet est impacté (intersecté) par la suppression d'un site
    ELSIF (TG_OP = 'INSERT') -- Si création d'un site
    OR 
    (
        (TG_OP = 'UPDATE') 
        AND ((SELECT ST_Equals(NEW.the_geom,OLD.the_geom)) IS FALSE)
    )
    -- Ou si modification géométrique d'un site
    THEN
		UPDATE atd16_economie.geo_lot_eco AS a SET idsite = NEW.gid WHERE st_intersects(NEW.the_geom, a.the_geom);
        UPDATE atd16_economie.geo_lot_eco AS a SET idsite = NULL WHERE (NEW.gid = a.idsite) AND NOT st_intersects(NEW.the_geom, a.the_geom);
		UPDATE atd16_economie.geo_destination_eco AS b SET idsite = NEW.gid WHERE st_intersects(NEW.the_geom, b.the_geom);
		UPDATE atd16_economie.geo_destination_eco AS b SET idsite = NULL WHERE (NEW.gid = b.idsite) AND NOT st_intersects(NEW.the_geom, b.the_geom);
		UPDATE atd16_economie.geo_etablissement_eco AS c SET idsite = NEW.gid WHERE st_intersects(NEW.the_geom, c.the_geom);
		UPDATE atd16_economie.geo_etablissement_eco AS c SET idsite = NULL WHERE (NEW.gid = c.idsite) AND NOT st_intersects(NEW.the_geom, c.the_geom);
        -- Mise à jour du champ idsite avec la valeur gid (site éco) lorsque l'objet est intersecté ou la valeur NULL lorsque l'objet n'est plus intersecté
        -- par la création ou la modification géographique d'un site
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_economie.f_maj_idsite_2() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_idsite_2() IS 
	'[ATD16] Mise à jour du champ idsite lorsqu''un site est créé, modifié ou supprimé';

-- #################################################################### Trigger #####################################################################

-- DROP TRIGGER "40_t_after_iud_maj_idsite_2" ON atd16_economie.geo_site_eco;

CREATE TRIGGER "40_t_after_iud_maj_idsite_2"
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_economie.geo_site_eco    
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_idsite_2();
    
COMMENT ON TRIGGER "40_t_after_iud_maj_idsite_2" ON atd16_economie.geo_site_eco IS 
    '[ATD16] Mise à jour du champ idsite lorsqu''un site est créée, modifiée ou supprimée';


-- ##################################################################################################################################################
-- ###                                      Mise à jour du nombre de lots (unités foncières) dans geo_site                                        ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_economie.f_maj_nb_unite_fonciere_2();

CREATE OR REPLACE FUNCTION atd16_economie.f_maj_nb_unite_fonciere_2()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    NEW.nb_unite_fonciere = (SELECT COUNT(b.gid) FROM atd16_economie.geo_lot_eco b WHERE ST_Intersects(b.the_geom,NEW.the_geom));
	--Le champ nb_unite_fonciere prend la valeur de la somme des lots étant intersectés avec le site créé ou modifié
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_economie.f_maj_nb_unite_fonciere_2() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_nb_unite_fonciere_2() IS 
    '[ATD16] Mise à jour du nombre d''unités foncières (lots) dans geo_site_eco lors de la création ou la modification d''un site';

-- #################################################################### Trigger #####################################################################

-- DROP TRIGGER "30_t_before_iu_maj_nb_unite_fonciere_2" ON atd16_economie.geo_site_eco;

CREATE TRIGGER "30_t_before_iu_maj_nb_unite_fonciere_2" 
    BEFORE INSERT OR UPDATE
    ON atd16_economie.geo_site_eco 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_nb_unite_fonciere_2();
    
COMMENT ON TRIGGER "30_t_before_iu_maj_nb_unite_fonciere_2" ON atd16_economie.geo_site_eco IS 
    '[ATD16] Mise à jour du nombre d''unités foncières (lots) dans geo_site_eco lors de la création ou la modification d''un site';


-- ##################################################################################################################################################
-- ###                                         Mise à jour du nombre d'établissements dans geo_site_eco                                           ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_economie.f_maj_nb_etablissement_2();

CREATE OR REPLACE FUNCTION atd16_economie.f_maj_nb_etablissement_2()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    NEW.nb_etablissement = (SELECT COUNT(b.gid) FROM atd16_economie.geo_etablissement_eco b WHERE ST_Intersects(b.the_geom,NEW.the_geom)); 
	--Le champ nb_etablissement prend la valeur de la somme des établissements étant intersectés avec le site créé ou modifié
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_economie.f_maj_nb_etablissement_2() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_nb_etablissement_2() IS 
    '[ATD16] Mise à jour du nombre d''établissements dans geo_site_eco lorsqu''un site est modifié ou créé';

-- #################################################################### Trigger #####################################################################

-- DROP TRIGGER "20_t_before_iu_maj_nb_etablissement_2" ON atd16_economie.geo_site_eco;

CREATE TRIGGER "20_t_before_iu_maj_nb_etablissement_2" 
    BEFORE INSERT OR UPDATE
    ON atd16_economie.geo_site_eco 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_nb_etablissement_2();
    
COMMENT ON TRIGGER "20_t_before_iu_maj_nb_etablissement_2" ON atd16_economie.geo_site_eco IS 
    '[ATD16] Mise à jour du nombre d''établissements dans geo_site_eco lorsqu''un site est modifié ou créé';


-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/10/06 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_maj_idlot_2() et du trigger associé 50_t_after_iud_maj_idlot_2
-- 2022/10/18 : SL / Ajout de la fonction f_maj_sup_foncier_dispo() et du trigger associé 70_t_after_iud_maj_sup_foncier_dispo
-- 2022/10/20 : SL / Ajout de la fonction f_maj_nb_unite_fonciere() et du trigger associé 60_t_after_iud_maj_nb_unite_fonciere
--                 . Correction de la fonction f_maj_sup_foncier_dispo()
--                 . Ajout de la fonction f_supp_valeur_idlot() et du trigger associé t_after_d_lot_supp_idlot
-- 2022/11/04 : SL / Correction de la fonction f_maj_idlot_2() : maj idlot si UPDATE géographique
-- 2022/11/09 : SL / Suppression de la fonction f_supp_valeur_idlot() et du trigger associé t_after_d_lot_supp_idlot
--                 . Ajout de la fonction f_maj_occupation_2() et du trigger associé 40_t_before_iu_maj_occupation_2

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                      Fonctions triggers et triggers spécifiques à la table geo_lot_eco                                     ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                    Mise à jour du lien idlot lorsqu'un lot est créé, modifié ou supprimé                                   ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_economie.f_maj_idlot_2();

CREATE OR REPLACE FUNCTION atd16_economie.f_maj_idlot_2()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
		UPDATE atd16_economie.geo_etablissement_eco AS c SET idlot = NULL WHERE st_intersects(OLD.the_geom, c.the_geom);
        -- Mise à jour du champ idlot avec la valeur NULL lorsque l'objet est impacté (intersecté) par la suppression d'un lot
    ELSIF (TG_OP = 'INSERT') -- Si création d'un lot
    OR 
    (
        (TG_OP = 'UPDATE') 
        AND ((SELECT ST_Equals(NEW.the_geom,OLD.the_geom)) IS FALSE)
    )
    THEN
		UPDATE atd16_economie.geo_etablissement_eco AS c SET idlot = NEW.gid WHERE st_intersects(NEW.the_geom, c.the_geom);
        UPDATE atd16_economie.geo_etablissement_eco AS c SET idlot = NULL WHERE (NEW.gid = c.idlot) AND NOT st_intersects(NEW.the_geom, c.the_geom);
        -- Mise à jour du champ idlot avec la valeur gid (lot éco) lorsque l'objet est intersecté ou NULL lorsqu'il n'est plus intersecté 
        -- par la création ou la modification d'un lot
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_economie.f_maj_idlot_2() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_idlot_2() IS 
	'[ATD16] Mise à jour du champ idlot lorsqu''un lot est créé, modifié ou supprimé';

-- #################################################################### Trigger #####################################################################

-- DROP TRIGGER "50_t_after_iud_maj_idlot_2" ON atd16_economie.geo_lot_eco;

CREATE TRIGGER "50_t_after_iud_maj_idlot_2"
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_economie.geo_lot_eco    
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_idlot_2();
    
COMMENT ON TRIGGER "50_t_after_iud_maj_idlot_2" ON atd16_economie.geo_lot_eco IS 
    '[ATD16] Mise à jour du champ idlot lorsqu''un lot est créée, modifiée ou supprimée';


-- ##################################################################################################################################################
-- ###                   Mise à jour du champ sup_foncier_dispo de la table geo_site lorsqu'un lot est créé, modifié ou supprimé                  ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_economie.f_maj_sup_foncier_dispo();

CREATE OR REPLACE FUNCTION atd16_economie.f_maj_sup_foncier_dispo()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
		UPDATE atd16_economie.geo_site_eco AS s -- Mise à jour de la table geo_site_eco
        SET sup_foncier_dispo = -- Où le champ sup_foncier_dispo prend la valeur
            (
                SELECT SUM(l.sup_m2) -- De la somme des surfaces des objets
                FROM atd16_economie.geo_lot_eco l -- De la table geo_lot_eco
                WHERE l.disponibilite = '01' -- Qui ont une disponibilité "Non bâti"
                AND l.idsite = OLD.idsite -- et un idsite identique avec celui qui est supprimé
            )
        WHERE gid::text = OLD.idsite::text; -- Où l'ancien idsite est égal au gid de la table geo_site_eco
    ELSE
		UPDATE atd16_economie.geo_site_eco AS s -- Mise à jour de la table geo_site_eco
        SET sup_foncier_dispo = -- Où le champ sup_foncier_dispo prend la valeur
            (
                SELECT SUM(l.sup_m2) -- De la somme des surfaces des objets
                FROM atd16_economie.geo_lot_eco l -- De la table geo_lot_eco
                WHERE l.disponibilite = '01' -- Qui ont une disponibilité "Non bâti"
                AND l.idsite = NEW.idsite -- et un idsite identique avec celui qui est créé ou modifié
            )
        WHERE gid::text = NEW.idsite::text; -- Où le nouvel idsite est égal au gid de la table geo_site_eco
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_economie.f_maj_sup_foncier_dispo() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_sup_foncier_dispo() IS 
	'[ATD16] Mise à jour du champ sup_foncier_dispo de la table geo_site lorsqu''un lot est créé, modifié ou supprimé';

-- #################################################################### Trigger #####################################################################

-- DROP TRIGGER "70_t_after_iud_maj_sup_foncier_dispo" ON atd16_economie.geo_lot_eco;

CREATE TRIGGER "70_t_after_iud_maj_sup_foncier_dispo"
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_economie.geo_lot_eco    
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_sup_foncier_dispo();
    
COMMENT ON TRIGGER "70_t_after_iud_maj_sup_foncier_dispo" ON atd16_economie.geo_lot_eco IS 
    '[ATD16] Mise à jour du champ sup_foncier_dispo lorsqu''un lot est créée, modifiée ou supprimée';


-- ##################################################################################################################################################
-- ###                                      Mise à jour du nombre de lots (unités foncières) dans geo_site                                        ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_economie.f_maj_nb_unite_fonciere();

CREATE OR REPLACE FUNCTION atd16_economie.f_maj_nb_unite_fonciere()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE atd16_economie.geo_site_eco -- On met à jour la table geo_site_eco
	    SET nb_unite_fonciere = --Le champ nb_unite_fonciere prend la valeur
	    (SELECT COUNT(b.gid) FROM atd16_economie.geo_lot_eco b WHERE b.idsite = OLD.idsite) 
	    --de la somme des lots ayant un idsite identique à celui qui est supprimé
	    WHERE gid::text = OLD.idsite::text; -- Où l'ancien idsite est égal au gid de la table geo_site_eco
    ELSE
        UPDATE atd16_economie.geo_site_eco -- On met à jour la table geo_site_eco
	    SET nb_unite_fonciere = --Le champ nb_unite_fonciere prend la valeur
	    (SELECT COUNT(b.gid) FROM atd16_economie.geo_lot_eco b WHERE b.idsite = NEW.idsite) 
	    --de la somme des lots ayant un idsite identique à celui qui est créé
	    WHERE gid::text = NEW.idsite::text; -- Où le nouvel idsite est égal au gid de la table geo_site_eco
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_economie.f_maj_nb_unite_fonciere() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_nb_unite_fonciere() IS 
    '[ATD16] Mise à jour du nombre d''unités foncières (lots) dans geo_site_eco lors de la suppression, modification ou création d''un lot';

-- #################################################################### Trigger #####################################################################

-- DROP TRIGGER "60_t_after_iud_maj_nb_unite_fonciere" ON atd16_economie.geo_lot_eco;

CREATE TRIGGER "60_t_after_iud_maj_nb_unite_fonciere" 
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_economie.geo_lot_eco 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_nb_unite_fonciere();
    
COMMENT ON TRIGGER "60_t_after_iud_maj_nb_unite_fonciere" ON atd16_economie.geo_lot_eco IS 
    '[ATD16] Mise à jour du nombre d''unités foncières (lots) dans geo_site_eco lors de la suppression, modification ou création d''un lot';


-- ##################################################################################################################################################
-- ###                                      Mise à jour du champ occupation lorsqu'un lot est créé ou modifié                                     ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_economie.f_maj_occupation_2();

CREATE OR REPLACE FUNCTION atd16_economie.f_maj_occupation_2()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF 
    (
        SELECT COUNT(*) FROM -- Calcul du nombre de ligne dans la table "etablot"
        (
            SELECT DISTINCT ST_Intersects(NEW.the_geom, etab.the_geom) FROM atd16_economie.geo_etablissement_eco etab
            -- Résultat des intersections entre le nouvel objet (lot) et les établissements. 
            -- Sans le DISTINCT, il y a autant de lignes que d'établissements.
            -- Le DISTINCT permet de regrouper les mêmes valeurs (2 valeurs possibles : TRUE ou FALSE)
        ) AS etablot -- nom de la table temporaire créé (indispensable)
    ) = 2 -- Si le nombre de ligne est égal à 2 (valeurs = false et true)
    THEN
		    NEW.occupation = TRUE;
            -- Mise à jour du champ occupation avec la valeur TRUE lorsque l'objet est intersecté avec un établissement
    ELSIF 
    (
        SELECT DISTINCT ST_Intersects(NEW.the_geom, etab.the_geom) FROM atd16_economie.geo_etablissement_eco etab
        -- Résultat des intersections entre le nouvel objet (lot) et les établissements. 
        -- Sans le DISTINCT, il y a autant de lignes que d'établissements.
        -- Le DISTINCT permet de regrouper les mêmes valeurs (2 valeurs possibles : TRUE ou FALSE)
    ) = TRUE -- Si la seule valeur est TRUE
    THEN
            NEW.occupation = TRUE;
            -- Mise à jour du champ occupation avec la valeur TRUE lorsque l'objet est intersecté avec un établissement
    ELSE
		    NEW.occupation = NULL;
            -- Mise à jour du champ occupation avec la valeur NULL lorsque l'objet n'est pas intersecté avec un établissement
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_economie.f_maj_occupation_2() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_occupation_2() IS 
	'[ATD16] Mise à jour du champ occupation lorsqu''un lot est créé ou modifié';

-- #################################################################### Trigger #####################################################################

-- DROP TRIGGER "40_t_before_iu_maj_occupation_2" ON atd16_economie.geo_lot_eco;

CREATE TRIGGER "40_t_before_iu_maj_occupation_2" 
    BEFORE INSERT OR UPDATE
    ON atd16_economie.geo_lot_eco 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_occupation_2();
    
COMMENT ON TRIGGER "40_t_before_iu_maj_occupation_2" ON atd16_economie.geo_lot_eco IS 
    '[ATD16] Mise à jour du champ occupation lors de la modification ou création d''un lot';


-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/10/20 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_maj_occupation() et du trigger associé 40_t_after_iud_maj_occupation
--                 . Ajout de la fonction f_maj_nb_etablissement() et du trigger associé 50_t_after_iud_maj_nb_etablissement

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                 Fonctions triggers et triggers spécifiques à la table geo_etablissement_eco                                ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                    Mise à jour du champ occupation de geo_lot_eco lorsqu'un établissement est créé, modifié ou supprimé                  ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_economie.f_maj_occupation();

CREATE OR REPLACE FUNCTION atd16_economie.f_maj_occupation()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
		UPDATE atd16_economie.geo_lot_eco AS c SET occupation = NULL WHERE st_intersects(OLD.the_geom, c.the_geom);
        -- Mise à jour du champ occupation avec la valeur NULL lorsque l'objet est impacté (intersecté) par la suppression d'un établissement
    ELSE
		UPDATE atd16_economie.geo_lot_eco AS c SET occupation = TRUE WHERE st_intersects(NEW.the_geom, c.the_geom);
        -- Mise à jour du champ occupation avec la valeur TRUE lorsque l'objet est impacté (intersecté) 
        -- par la création ou la modification d'un établissement
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_economie.f_maj_occupation() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_occupation() IS 
	'[ATD16] Mise à jour du champ occupation lorsqu''un établissement est créé, modifié ou supprimé';

-- #################################################################### Trigger #####################################################################

-- DROP TRIGGER "40_t_after_iud_maj_occupation" ON atd16_economie.geo_etablissement_eco;

CREATE TRIGGER "40_t_after_iud_maj_occupation"
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_economie.geo_etablissement_eco    
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_occupation();
    
COMMENT ON TRIGGER "40_t_after_iud_maj_occupation" ON atd16_economie.geo_etablissement_eco IS 
    '[ATD16] Mise à jour du champ occupation lorsqu''un établissement est créée, modifiée ou supprimée';


-- ##################################################################################################################################################
-- ###                                           Mise à jour du nombre d'établissements dans geo_site_eco                                             ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_economie.f_maj_nb_etablissement();

CREATE OR REPLACE FUNCTION atd16_economie.f_maj_nb_etablissement()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE atd16_economie.geo_site_eco -- On met à jour la table geo_site_eco
	    SET nb_etablissement = --Le champ nb_etablissement prend la valeur
	    (SELECT COUNT(b.gid) FROM atd16_economie.geo_etablissement_eco b WHERE b.idsite = OLD.idsite) 
	    --de la somme des établissements ayant un idsite identique à celui qui est supprimé
	    WHERE gid::text = OLD.idsite::text; -- Où l'ancien idsite est égal au gid de la table geo_site_eco
    ELSE
        UPDATE atd16_economie.geo_site_eco -- On met à jour la table geo_site_eco
	    SET nb_etablissement = --Le champ nb_etablissement prend la valeur
	    (SELECT COUNT(b.gid) FROM atd16_economie.geo_etablissement_eco b WHERE b.idsite = NEW.idsite) 
	    --de la somme des établissements ayant un idsite identique à celui qui est créé
	    WHERE gid::text = NEW.idsite::text; -- Où le nouvel idsite est égal au gid de la table geo_site_eco
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_economie.f_maj_nb_etablissement() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_nb_etablissement() IS 
    '[ATD16] Mise à jour du nombre d''établissements dans geo_site_eco lorsqu''un établissement est supprimé, modifié ou créé';

-- #################################################################### Trigger #####################################################################

-- DROP TRIGGER "50_t_after_iud_maj_nb_etablissement" ON atd16_economie.geo_etablissement_eco;

CREATE TRIGGER "50_t_after_iud_maj_nb_etablissement"
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_economie.geo_etablissement_eco 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_nb_etablissement();
    
COMMENT ON TRIGGER "50_t_after_iud_maj_nb_etablissement" ON atd16_economie.geo_etablissement_eco IS 
    '[ATD16] Mise à jour du nombre d''établissements dans geo_site_eco lorsqu''un établissement est supprimé, modifié ou créé';

