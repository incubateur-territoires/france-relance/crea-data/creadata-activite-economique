-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/10/05 : SL / Création du fichier sur Git
--                 . Ajout du trigger 00_t_before_i_init_date_creation
--                 . Ajout du trigger 02_t_before_u_date_maj
--                 . Ajout du trigger 10_t_before_iu_maj_xy_coord
--                 . Ajout du trigger 20_t_before_iu_maj_idsite
--                 . Ajout du trigger 30_t_before_iu_maj_idlot
-- 2022/10/20 : SL / Correction du trigger 02_t_before_u_date_maj
--                 . Ajout du trigger t_after_iud_maj_taux_vacance
-- 2022/11/09 : SL / Suppression du trigger t_after_iud_maj_taux_vacance

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                               Table géographique : Établissement économique                                                ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE atd16_economie.geo_etablissement_eco;

CREATE TABLE atd16_economie.geo_etablissement_eco
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    idsite integer, --[FK][ATD16] Identifiant du site économique
    idlot integer, --[FK][ATD16] Identifiant du lot économique
    insee varchar(6), --[ATD16] Code INSEE de la commune
    ident varchar(254), --[SIRAP] Nom de l'établissement - Identifiant SIRAP
    x_l93 numeric(10,3), --[ATD16] Coordonnées X en Lambert 93 (automatique)
    y_l93 numeric(10,3), --[ATD16] Coordonnées Y en Lambert 93 (automatique)
    date_creation timestamp, --[ATD16] Date de création de l'objet (automatique)
    date_maj timestamp, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_etablissement_eco PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE atd16_economie.geo_etablissement_eco OWNER TO sditecgrp;

GRANT ALL ON TABLE atd16_economie.geo_etablissement_eco TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_economie.geo_etablissement_eco IS '[ATD16] Table contenant la géographie des établissements économiques';

COMMENT ON COLUMN atd16_economie.geo_etablissement_eco.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN atd16_economie.geo_etablissement_eco.idsite IS '[FK][ATD16] Identifiant du site économique';
COMMENT ON COLUMN atd16_economie.geo_etablissement_eco.idlot IS '[FK][ATD16] Identifiant du lot économique';
COMMENT ON COLUMN atd16_economie.geo_etablissement_eco.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN atd16_economie.geo_etablissement_eco.ident IS '[SIRAP] Nom de l''établissement - Identifiant SIRAP';
COMMENT ON COLUMN atd16_economie.geo_etablissement_eco.x_l93 IS '[ATD16] Coordonnées X en Lambert 93 (automatique)';
COMMENT ON COLUMN atd16_economie.geo_etablissement_eco.y_l93 IS '[ATD16] Coordonnées Y en Lambert 93 (automatique)';
COMMENT ON COLUMN atd16_economie.geo_etablissement_eco.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_economie.geo_etablissement_eco.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_economie.geo_etablissement_eco.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################



-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                          Trigger(s) générique(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER "00_t_before_i_init_date_creation" ON atd16_economie.geo_etablissement_eco;

CREATE TRIGGER "00_t_before_i_init_date_creation"
    BEFORE INSERT
    ON atd16_economie.geo_etablissement_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_date_creation();
    
COMMENT ON TRIGGER "00_t_before_i_init_date_creation" ON atd16_economie.geo_etablissement_eco IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER "02_t_before_u_date_maj" ON atd16_economie.geo_etablissement_eco;

CREATE TRIGGER "02_t_before_u_date_maj"
    BEFORE UPDATE
    ON atd16_economie.geo_etablissement_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_date_maj();
    
COMMENT ON TRIGGER "02_t_before_u_date_maj" ON atd16_economie.geo_etablissement_eco IS
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                    Mise à jour des champs x_l93 et y_l93                                                   ###
-- ##################################################################################################################################################

-- DROP TRIGGER "10_t_before_iu_maj_xy_coord" ON atd16_economie.geo_etablissement_eco;

CREATE TRIGGER "10_t_before_iu_maj_xy_coord"
    BEFORE INSERT OR UPDATE
    ON atd16_economie.geo_etablissement_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_xy_coord();
    
COMMENT ON TRIGGER "10_t_before_iu_maj_xy_coord" ON atd16_economie.geo_etablissement_eco IS 
    'Trigger déclenchant la fonction permettant la mise à jour des champs x_l93 et y_l93';


-- ##################################################################################################################################################
-- ###                                                        Mise à jour du champ idsite                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER "20_t_before_iu_maj_idsite" ON atd16_economie.geo_etablissement_eco;

CREATE TRIGGER "20_t_before_iu_maj_idsite"
    BEFORE INSERT OR UPDATE
    ON atd16_economie.geo_etablissement_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_idsite();
    
COMMENT ON TRIGGER "20_t_before_iu_maj_idsite" ON atd16_economie.geo_etablissement_eco IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idsite';


-- ##################################################################################################################################################
-- ###                                                         Mise à jour du champ idlot                                                         ###
-- ##################################################################################################################################################

-- DROP TRIGGER "30_t_before_iu_maj_idlot" ON atd16_economie.geo_etablissement_eco;

CREATE TRIGGER "30_t_before_iu_maj_idlot"
    BEFORE INSERT OR UPDATE
    ON atd16_economie.geo_etablissement_eco
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_idlot();
    
COMMENT ON TRIGGER "30_t_before_iu_maj_idlot" ON atd16_economie.geo_etablissement_eco IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ idlot';


