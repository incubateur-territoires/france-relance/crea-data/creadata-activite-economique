-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/10/20 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_maj_occupation() et du trigger associé 40_t_after_iud_maj_occupation
--                 . Ajout de la fonction f_maj_nb_etablissement() et du trigger associé 50_t_after_iud_maj_nb_etablissement

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                 Fonctions triggers et triggers spécifiques à la table geo_etablissement_eco                                ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                    Mise à jour du champ occupation de geo_lot_eco lorsqu'un établissement est créé, modifié ou supprimé                  ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_economie.f_maj_occupation();

CREATE OR REPLACE FUNCTION atd16_economie.f_maj_occupation()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
		UPDATE atd16_economie.geo_lot_eco AS c SET occupation = NULL WHERE st_intersects(OLD.the_geom, c.the_geom);
        -- Mise à jour du champ occupation avec la valeur NULL lorsque l'objet est impacté (intersecté) par la suppression d'un établissement
    ELSE
		UPDATE atd16_economie.geo_lot_eco AS c SET occupation = TRUE WHERE st_intersects(NEW.the_geom, c.the_geom);
        -- Mise à jour du champ occupation avec la valeur TRUE lorsque l'objet est impacté (intersecté) 
        -- par la création ou la modification d'un établissement
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_economie.f_maj_occupation() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_occupation() IS 
	'[ATD16] Mise à jour du champ occupation lorsqu''un établissement est créé, modifié ou supprimé';

-- #################################################################### Trigger #####################################################################

-- DROP TRIGGER "40_t_after_iud_maj_occupation" ON atd16_economie.geo_etablissement_eco;

CREATE TRIGGER "40_t_after_iud_maj_occupation"
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_economie.geo_etablissement_eco    
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_occupation();
    
COMMENT ON TRIGGER "40_t_after_iud_maj_occupation" ON atd16_economie.geo_etablissement_eco IS 
    '[ATD16] Mise à jour du champ occupation lorsqu''un établissement est créée, modifiée ou supprimée';


-- ##################################################################################################################################################
-- ###                                           Mise à jour du nombre d'établissements dans geo_site_eco                                             ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

-- DROP FUNCTION atd16_economie.f_maj_nb_etablissement();

CREATE OR REPLACE FUNCTION atd16_economie.f_maj_nb_etablissement()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE -- Permet de déclarer des variables
    
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE atd16_economie.geo_site_eco -- On met à jour la table geo_site_eco
	    SET nb_etablissement = --Le champ nb_etablissement prend la valeur
	    (SELECT COUNT(b.gid) FROM atd16_economie.geo_etablissement_eco b WHERE b.idsite = OLD.idsite) 
	    --de la somme des établissements ayant un idsite identique à celui qui est supprimé
	    WHERE gid::text = OLD.idsite::text; -- Où l'ancien idsite est égal au gid de la table geo_site_eco
    ELSE
        UPDATE atd16_economie.geo_site_eco -- On met à jour la table geo_site_eco
	    SET nb_etablissement = --Le champ nb_etablissement prend la valeur
	    (SELECT COUNT(b.gid) FROM atd16_economie.geo_etablissement_eco b WHERE b.idsite = NEW.idsite) 
	    --de la somme des établissements ayant un idsite identique à celui qui est créé
	    WHERE gid::text = NEW.idsite::text; -- Où le nouvel idsite est égal au gid de la table geo_site_eco
    END IF;
    RETURN NEW;
END;
    
$BODY$;

ALTER FUNCTION atd16_economie.f_maj_nb_etablissement() OWNER TO sditecgrp;

COMMENT ON FUNCTION atd16_economie.f_maj_nb_etablissement() IS 
    '[ATD16] Mise à jour du nombre d''établissements dans geo_site_eco lorsqu''un établissement est supprimé, modifié ou créé';

-- #################################################################### Trigger #####################################################################

-- DROP TRIGGER "50_t_after_iud_maj_nb_etablissement" ON atd16_economie.geo_etablissement_eco;

CREATE TRIGGER "50_t_after_iud_maj_nb_etablissement"
    AFTER INSERT OR UPDATE OR DELETE
    ON atd16_economie.geo_etablissement_eco 
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_economie.f_maj_nb_etablissement();
    
COMMENT ON TRIGGER "50_t_after_iud_maj_nb_etablissement" ON atd16_economie.geo_etablissement_eco IS 
    '[ATD16] Mise à jour du nombre d''établissements dans geo_site_eco lorsqu''un établissement est supprimé, modifié ou créé';

